package com.gm.cleanarchbp.presentation.common.viewmodel

import androidx.lifecycle.ViewModel

open class BaseViewModel : ViewModel()
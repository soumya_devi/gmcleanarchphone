package com.gm.cleanarchbp.presentation.common.branding

import android.view.View
import androidx.annotation.StringRes
import com.gm.core.view.BigSnackbar
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject

class CadillacErrorStrategy @Inject constructor() : ErrorStrategy {

    override fun showError(view: View, string: String) {
        BigSnackbar.make(view, string, Snackbar.LENGTH_LONG).show()
    }

    override fun showError(view: View, @StringRes stringId: Int) {
        showError(view, view.context.getString(stringId))
    }
}
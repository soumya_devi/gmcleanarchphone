/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.presentation.common.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.gm.cleanarchbp.presentation.common.branding.ScreenType
import com.gm.core.view.RtlSupport
import dagger.android.AndroidInjection
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import javax.inject.Inject

abstract class BaseActivity : AppCompatActivity(), HasAndroidInjector {
    // This object is needed to enable fragment to know about the activity dependencies
    @Inject
    internal lateinit var mAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    internal lateinit var mRtlSupport: RtlSupport

    abstract val screenType: ScreenType

    override fun onCreate(savedInstanceState: Bundle?) {
        //This provides every object annotated with @Inject
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
    }

    override fun androidInjector(): AndroidInjector<Any> {
        return mAndroidInjector
    }
}

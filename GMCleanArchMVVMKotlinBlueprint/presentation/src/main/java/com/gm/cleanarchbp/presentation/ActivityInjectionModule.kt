/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */
package com.gm.cleanarchbp.presentation

import com.gm.cleanarchbp.presentation.main.MainActivity
import com.gm.cleanarchbp.presentation.main.MainActivityModule
import com.gm.core.annotation.ActivityScope

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
internal abstract class ActivityInjectionModule {

    @ActivityScope
    @ContributesAndroidInjector(modules = [MainActivityModule::class])
    internal abstract fun mainActivityInjector(): MainActivity
}

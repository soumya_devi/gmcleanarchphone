/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.gm.cleanarchbp.presentation.viewmodel.chrono.ChronoViewModel
import com.gm.cleanarchbp.presentation.viewmodel.chrono.H2G2ViewModel
import com.gm.cleanarchbp.presentation.viewmodel.h2g2.ContactsViewModel
import com.gm.core.ViewModelFactory
import com.gm.core.annotation.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
internal abstract class ViewModelModule {

    @Binds
    internal abstract fun viewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ChronoViewModel::class)
    internal abstract fun mainViewModel(viewModel: ChronoViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(H2G2ViewModel::class)
    internal abstract fun h2g2ViewModel(viewModel: H2G2ViewModel): ViewModel

    @Binds
    @IntoMap
    @ViewModelKey(ContactsViewModel::class)
    internal abstract fun contactsViewModel(viewModel: ContactsViewModel): ViewModel
}
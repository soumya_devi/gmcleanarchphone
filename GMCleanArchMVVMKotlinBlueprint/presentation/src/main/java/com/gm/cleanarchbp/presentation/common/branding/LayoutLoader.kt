package com.gm.cleanarchbp.presentation.common.branding

import androidx.annotation.LayoutRes
import com.gm.cleanarchbp.R
import com.gm.cleanarchbp.model.entity.BrandType
import com.gm.cleanarchbp.model.util.ConfigSupport
import com.gm.cleanarchbp.presentation.common.view.BaseActivity
import javax.inject.Inject

class LayoutLoader @Inject
constructor(private val configSupport: ConfigSupport) {

    fun setContentView(activity: BaseActivity) {
        val brandType = configSupport.getCurrentBrand()
        @LayoutRes val layoutId: Int = when (activity.screenType) {
            ScreenType.MAIN -> loadMainActivityLayout(brandType)
        }
        activity.setContentView(layoutId)
    }

    @LayoutRes
    private fun loadMainActivityLayout(brandType: BrandType): Int =
            when (brandType) {
              //  BrandType.CADILLAC_FREE_FORM -> R.layout.activity_main_cadillac
                BrandType.CADILLAC -> R.layout.activity_main
              //  BrandType.CHEVY -> R.layout.activity_main_chevy
               // BrandType.GMC -> R.layout.activity_main_gmc
            }
}

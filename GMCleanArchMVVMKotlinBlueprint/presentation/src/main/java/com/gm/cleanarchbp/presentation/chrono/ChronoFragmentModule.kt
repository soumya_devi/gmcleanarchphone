package com.gm.cleanarchbp.presentation.chrono

import com.gm.cleanarchbp.presentation.common.BaseFragmentModule
import dagger.Module


@Module(includes = [BaseFragmentModule::class])
abstract class ChronoFragmentModule
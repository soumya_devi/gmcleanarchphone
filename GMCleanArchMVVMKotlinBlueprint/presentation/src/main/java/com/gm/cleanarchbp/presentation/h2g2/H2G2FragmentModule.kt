package com.gm.cleanarchbp.presentation.h2g2

import com.gm.cleanarchbp.presentation.common.BaseFragmentModule
import dagger.Module


@Module(includes = [BaseFragmentModule::class])
abstract class H2G2FragmentModule
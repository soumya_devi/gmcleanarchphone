/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.presentation.h2g2

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gm.cleanarchbp.R
import com.gm.cleanarchbp.presentation.common.view.BaseFragment
import com.gm.cleanarchbp.presentation.viewmodel.chrono.H2G2ViewModel
import com.gm.cleanarchbp.presentation.viewmodel.h2g2.DataClass
import com.gm.core.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_tabs.*
import javax.inject.Inject


// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [TabsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [TabsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TabsFragment : BaseFragment() {

    private lateinit var viewModel: H2G2ViewModel

    @Inject
    internal lateinit var viewModelFactory: ViewModelFactory
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view= inflater.inflate(R.layout.fragment_tabs, container, false)
      //  val recycle:RecyclerView=view.findViewById(R.id.recycle)
        //viewModel = ViewModelProviders.of(baseActivity, viewModelFactory)[H2G2ViewModel::class.java]
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recyclerViewTabs()
        super.onViewCreated(view, savedInstanceState)

    }
    private fun recyclerViewTabs(){
       // recycle.layoutManager=LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false)
val data=ArrayList<DataClass>()
        data.add(DataClass("Recents"))
        data.add(DataClass("Contacts"))
        data.add(DataClass("Keypad"))
        data.add(DataClass("Phone"))
        //al adapter=RecyclerViewAdapter(data)
      //  recycle.adapter=adapter
    }
}


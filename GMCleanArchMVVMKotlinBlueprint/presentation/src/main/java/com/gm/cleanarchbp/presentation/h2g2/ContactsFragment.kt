package com.gm.cleanarchbp.presentation.h2g2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.gm.cleanarchbp.R
import com.gm.cleanarchbp.model.entity.PBContactsListDetails
import com.gm.cleanarchbp.presentation.common.branding.ErrorStrategy
import com.gm.cleanarchbp.presentation.common.view.BaseFragment
import com.gm.cleanarchbp.presentation.viewmodel.h2g2.ContactsViewModel
import com.gm.cleanarchbp.presentation.viewmodel.h2g2.DataClass
import com.gm.core.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_contacts.*
import kotlinx.android.synthetic.main.fragment_h2g2.*
import javax.inject.Inject


/**
 * A simple [Fragment] subclass.
 * Activities that contain this fragment must implement the
 * [ContactsFragment.OnFragmentInteractionListener] interface
 * to handle interaction events.
 * Use the [ContactsFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class ContactsFragment : BaseFragment() {


    @Inject
    internal lateinit var viewModelFactory: ViewModelFactory

    @Inject
    internal lateinit var errorStrategy: ErrorStrategy

    private lateinit var viewModel: ContactsViewModel
    val data = ArrayList<DataClass>()


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment

        val rootView = inflater.inflate(R.layout.fragment_contacts, container, false)
        viewModel = ViewModelProviders.of(baseActivity, viewModelFactory)[ContactsViewModel::class.java]
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeToFeatureNameStateChanges()
        recyclerViewTabs()
    }

    private fun recyclerViewTabs() {
        contacts_recycle.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        val adapter = RecyclerViewAdapter(data)
        contacts_recycle.adapter = adapter
    }

    private fun subscribeToFeatureNameStateChanges() {
        viewModel.findSomeFeature().observe(viewLifecycleOwner, Observer<PBContactsListDetails> {
            data.add(DataClass(it.firstName))
        })
    }

}

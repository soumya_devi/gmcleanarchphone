package com.gm.cleanarchbp.presentation.chrono

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.gm.cleanarchbp.R
import com.gm.cleanarchbp.presentation.common.view.BaseFragment
import com.gm.cleanarchbp.presentation.viewmodel.chrono.ChronoViewModel
import com.gm.core.ViewModelFactory
import kotlinx.android.synthetic.main.fragment_chrono.*
import javax.inject.Inject


class ChronoFragment : BaseFragment() {

    @Inject
    internal lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: ChronoViewModel

    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_chrono, container, false)
        viewModel = ViewModelProviders.of(baseActivity, viewModelFactory)[ChronoViewModel::class.java]
        return rootView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeToFeatureEventStateChanges()
        listenToClickEvents()
    }

    private fun subscribeToFeatureEventStateChanges() {
        viewModel.chronoEventData.observe(viewLifecycleOwner, Observer {
            val eventString = resources.getString(R.string.event_feature)
            chronoEventTextView.text = String.format(eventString, it.name, it.value)
        })
        viewModel.chronoEventIsLoading.observe(viewLifecycleOwner, Observer {
            chronoEventTextView.setText(R.string.event_loading)
        })
    }

    private fun listenToClickEvents() {
        resetChronoButton.setOnClickListener { viewModel.resetChrono() }
    }
}
/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.presentation

import com.gm.cleanarchbp.model.usecase.ListenToChronoUpdatesUseCase
import com.gm.cleanarchbp.presentation.util.HMILog
import javax.inject.Inject

class CleanArchBPController @Inject constructor(
        private val listenToChronoUpdatesUseCase: ListenToChronoUpdatesUseCase) {

    fun startListeningToChronoUpdates() {
        listenToChronoUpdatesUseCase.execute({
            HMILog.d("Started listening to event feature")
        }, {
            HMILog.e("Error while listening to event feature", it)
        })
    }
}

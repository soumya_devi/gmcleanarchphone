/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */
package com.gm.cleanarchbp.presentation

import android.content.Context
import com.gm.cleanarchbp.model.entity.BrandType
import com.gm.cleanarchbp.model.util.ConfigSupport
import com.gm.cleanarchbp.presentation.common.branding.CadillacErrorStrategy
import com.gm.cleanarchbp.presentation.common.branding.DefaultErrorStrategy
import com.gm.cleanarchbp.presentation.common.branding.ErrorStrategy

import com.gm.core.annotation.PresentationContext
import com.gm.core.annotation.PresentationScope

import dagger.Binds
import dagger.Module
import dagger.Provides

/**
 * Responsible for providing objects which can be injected (exposed to the
 * dependency graph). Returned objects from methods with @Binds annotation are available for
 * dependency injection.
 *
 * @ActivityScope abstract EventBus eventBus(final RxBus eventBus);
 *
 *
 * is equivalent to:
 * @Provides
 * @ActivityScope EventBus createEventBus(...) {
 * return new RxBus(...);
 * }
 * @PresentationScope annotation ensures that only one instance of that implementation is
 * created across the presentation module. This uniqueness will be handled by dagger generated code.
 *
 *
 * All the objects in this class can be injected in the contact list fragment.
 */
@Module
abstract class PresentationModule {

    @Binds
    @PresentationScope
    @PresentationContext
    internal abstract fun presentationContext(application: CleanArchBPApplication): Context

    @Module
    companion object {
        @Provides
        @JvmStatic
        @PresentationScope
        fun provideErrorHandler(configSupport: ConfigSupport)
                : ErrorStrategy {
            return when (configSupport.getCurrentBrand()) {
              //  BrandType.CADILLAC_FREE_FORM -> CadillacErrorStrategy()
                BrandType.CADILLAC -> CadillacErrorStrategy()
               // BrandType.CHEVY -> DefaultErrorStrategy()
              //  BrandType.GMC -> DefaultErrorStrategy()
            }
        }
    }
}

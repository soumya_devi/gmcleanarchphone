package com.gm.cleanarchbp.presentation.viewmodel.h2g2

import androidx.lifecycle.MutableLiveData
import com.gm.cleanarchbp.model.entity.PBContactsListDetails
import com.gm.cleanarchbp.model.usecase.FindTheAnswerToTheUniverseUseCase
import com.gm.cleanarchbp.presentation.common.viewmodel.BaseViewModel
import javax.inject.Inject

class ContactsViewModel @Inject
constructor(
        private val findTheAnswerToTheUniverseUseCase: FindTheAnswerToTheUniverseUseCase)
    : BaseViewModel() {
    private val featureUiData = MutableLiveData<PBContactsListDetails>()


    fun findSomeFeature(): MutableLiveData<PBContactsListDetails> {
        featureUiData.value = findTheAnswerToTheUniverseUseCase.contactList()[0]
        return featureUiData

    }

    override fun onCleared() {
        super.onCleared()
        findTheAnswerToTheUniverseUseCase.clear()
    }

}
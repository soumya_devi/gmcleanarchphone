package com.gm.cleanarchbp.presentation.h2g2

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.gm.cleanarchbp.R
import com.gm.cleanarchbp.model.entity.PBContactsListDetails
import com.gm.cleanarchbp.presentation.RecyclerViewClickListener
import com.gm.cleanarchbp.presentation.common.branding.ErrorStrategy
import com.gm.cleanarchbp.presentation.common.view.BaseFragment
import com.gm.cleanarchbp.presentation.viewmodel.chrono.H2G2ViewModel
import com.gm.cleanarchbp.presentation.viewmodel.h2g2.DataClass
import com.gm.cleanarchbp.presentation.viewmodel.h2g2.H2G2TheAnswerUiData
import com.gm.core.ViewModelFactory
import com.gm.core.viewmodel.OneShotObserver
import kotlinx.android.synthetic.main.fragment_h2g2.*
import javax.inject.Inject
import com.gm.cleanarchbp.presentation.main.MainActivity


class H2G2Fragment : BaseFragment() {


//    override fun recyclerViewListClicked(v: View, position: Int) {
//        //val adapter=RecyclerViewAdapter(data,this)
//    }

    @Inject
    internal lateinit var viewModelFactory: ViewModelFactory

    @Inject
    internal lateinit var errorStrategy: ErrorStrategy

    private lateinit var viewModel: H2G2ViewModel
    val data = ArrayList<DataClass>()
    override fun onCreateView(inflater: LayoutInflater,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_h2g2, container, false)
        viewModel = ViewModelProviders.of(baseActivity, viewModelFactory)[H2G2ViewModel::class.java]
        return rootView
    }

    private fun recyclerViewTabs() {
        recycle.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)

        data.add(DataClass("Recents"))
        data.add(DataClass("Contacts"))
        data.add(DataClass("Keypad"))
        data.add(DataClass("Phone"))
        val adapter = RecyclerViewAdapter(data)
        recycle.adapter = adapter

        recycle.addOnItemTouchListener(RecyclerTouchListener(activity!!.applicationContext, recycle, object : RecyclerTouchListener.ClickListener {
            override fun onClick(view: View, position: Int) {
                (activity as MainActivity).replaceFragment(position)
            }

            override fun onLongClick(view: View, position: Int) {

            }
        }))


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerViewTabs()


    }


}
/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.presentation.main

import android.os.Bundle
import com.gm.cleanarchbp.presentation.common.branding.LayoutLoader
import com.gm.cleanarchbp.presentation.common.branding.ScreenType
import com.gm.cleanarchbp.presentation.common.view.BaseActivity
import javax.inject.Inject
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager


class MainActivity : BaseActivity() {

    @Inject
    internal lateinit var layoutLoader: LayoutLoader

    override val screenType: ScreenType
        get() = ScreenType.MAIN

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layoutLoader.setContentView(this)
    }

    fun replaceFragment(pos: Int) {
      Log.d("siddu",""+pos)
    }


}

/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.presentation.h2g2

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.gm.cleanarchbp.model.usecase.FindTheAnswerToTheUniverseUseCase
import com.gm.cleanarchbp.model.usecase.model.AnswerFound
import com.gm.cleanarchbp.model.usecase.model.AnswerNotFound
import com.gm.cleanarchbp.model.usecase.model.FindSomeFeatureResult
import com.gm.cleanarchbp.presentation.common.viewmodel.BaseViewModel
import com.gm.cleanarchbp.presentation.common.viewmodel.ReloadableLiveData
import com.gm.cleanarchbp.presentation.common.viewmodel.UpdateIfChangedLiveData
import com.gm.cleanarchbp.presentation.util.HMILog
import com.gm.cleanarchbp.presentation.viewmodel.h2g2.H2G2TheAnswerUiData
import com.gm.core.viewmodel.OneShot
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

    /**
     * It tells the view what to map, show or hide. The presenter does not know directly if the view
     * is an activity or a fragment and it shall never be the case.
     * This class should have the minimum amount of android related things. If it has access to a
     * context, it shall only be to retrieve configuration values in res/xml/config.xml for example
     */
    class TabsViewModel @Inject
    constructor(
            private val findTheAnswerToTheUniverseUseCase: FindTheAnswerToTheUniverseUseCase)
        : BaseViewModel() {

        private val _featureIsLoading = UpdateIfChangedLiveData<Boolean>()
        val featureIsLoading: LiveData<Boolean> get() = _featureIsLoading

        private val _featureData = ReloadableLiveData<H2G2TheAnswerUiData>({
            findSomeFeature()
        })
        val featureUiData: LiveData<H2G2TheAnswerUiData> get() = _featureData

        private val _featureError = MutableLiveData<OneShot<Unit>>()
        val featureError: LiveData<OneShot<Unit>> get() = _featureError

        fun findSomeFeature() {
            _featureIsLoading.value = true
            findTheAnswerToTheUniverseUseCase.execute(FindFeatureObserver())
        }

        override fun onCleared() {
            super.onCleared()
            findTheAnswerToTheUniverseUseCase.clear()
        }

        private inner class FindFeatureObserver : DisposableSingleObserver<FindSomeFeatureResult>() {
            override fun onSuccess(featureResult: FindSomeFeatureResult) {
                _featureIsLoading.value = false
                when (featureResult) {
                    is AnswerFound ->
                        _featureData.value = H2G2TheAnswerUiData(
                                featureResult.deepThoughtAnswer.name,
                                featureResult.deepThoughtAnswer.value)
                    AnswerNotFound ->
                        _featureError.value = OneShot(Unit)
                }
            }

            override fun onError(e: Throwable) {
                _featureIsLoading.value = false
                _featureError.value = OneShot(Unit)
                HMILog.e("Error while executing " + findTheAnswerToTheUniverseUseCase.javaClass.simpleName, e)
            }
        }
    }


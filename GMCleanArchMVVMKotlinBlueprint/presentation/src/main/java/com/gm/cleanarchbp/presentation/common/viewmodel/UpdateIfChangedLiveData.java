package com.gm.cleanarchbp.presentation.common.viewmodel;

import androidx.lifecycle.MutableLiveData;

import java.util.Objects;

public class UpdateIfChangedLiveData<T> extends MutableLiveData<T> {
    @Override
    public void postValue(T value) {
        if (!Objects.equals(value, getValue())) {
            super.postValue(value);
        }
    }

    @Override
    public void setValue(T value) {
        if (!Objects.equals(value, getValue())) {
            super.setValue(value);
        }
    }
}

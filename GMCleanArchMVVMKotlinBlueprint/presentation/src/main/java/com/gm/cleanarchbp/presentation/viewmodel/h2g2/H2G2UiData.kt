package com.gm.cleanarchbp.presentation.viewmodel.h2g2

sealed class H2G2UiData
data class H2G2TheAnswerUiData(val name: String, val value: Long) : H2G2UiData()


//data class PBContactsListDetails(var contactHandle: Int, var firstName: String, var lastName: String, var phoneNumber: String , var contactHeaderDetailsMap: LinkedHashMap<String, Int>?)


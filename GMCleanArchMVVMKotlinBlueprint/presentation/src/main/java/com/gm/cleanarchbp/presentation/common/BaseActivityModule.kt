/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.presentation.common

import android.app.Activity
import android.content.Context

import com.gm.cleanarchbp.presentation.common.view.BaseActivity
import com.gm.core.annotation.ActivityContext
import com.gm.core.annotation.ActivityScope

import dagger.Binds
import dagger.Module

@Module
abstract class BaseActivityModule {

    @Binds
    @ActivityContext
    @ActivityScope
    internal abstract fun activityContext(activity: Activity): Context

    @Binds
    @ActivityScope
    internal abstract fun baseActivity(mainActivity: BaseActivity): Activity
}

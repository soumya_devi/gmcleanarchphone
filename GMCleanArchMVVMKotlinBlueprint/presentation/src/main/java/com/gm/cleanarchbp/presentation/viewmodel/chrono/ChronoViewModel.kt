/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */
package com.gm.cleanarchbp.presentation.viewmodel.chrono

import androidx.lifecycle.LiveData
import com.gm.cleanarchbp.model.entity.event.ChronoEvent
import com.gm.cleanarchbp.model.usecase.ResetChronoUseCase
import com.gm.cleanarchbp.presentation.common.viewmodel.BaseViewModel
import com.gm.cleanarchbp.presentation.common.viewmodel.UpdateIfChangedLiveData
import com.gm.cleanarchbp.presentation.util.HMILog
import com.gm.core.event.EventBus
import com.gm.core.event.EventSubscriber
import javax.inject.Inject


class ChronoViewModel @Inject
constructor(
        private val eventBus: EventBus,
        private val resetChronoUseCase: ResetChronoUseCase)
    : BaseViewModel(), EventSubscriber {

    private val _chronoEventIsLoading = UpdateIfChangedLiveData<Boolean>()
    val chronoEventIsLoading: LiveData<Boolean> get() = _chronoEventIsLoading

    private val _chronoEventData = UpdateIfChangedLiveData<ChronoEventUiData>()
    val chronoEventData: LiveData<ChronoEventUiData> get() = _chronoEventData

    init {
        _chronoEventIsLoading.value = true
        eventBus.register(ChronoEvent::class.java, this) { featureEvent ->
            _chronoEventIsLoading.value = false
            val chrono = featureEvent.chrono
            _chronoEventData.value = ChronoEventUiData(chrono.text, chrono.value)
        }
    }

    fun resetChrono() {
        resetChronoUseCase.execute({
            HMILog.d("Chrono has been reset")
        }, {
            HMILog.e("An error occurred while attempting to reset the chrono")
        })
    }

    override fun onCleared() {
        super.onCleared()
        eventBus.unregister(this)
        resetChronoUseCase.clear()
    }
}

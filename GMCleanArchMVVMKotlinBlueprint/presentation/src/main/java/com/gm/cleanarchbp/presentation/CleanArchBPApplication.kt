/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.presentation

import android.app.Application
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.OnLifecycleEvent
import com.gm.cleanarchbp.model.ModelComponent
import com.gm.cleanarchbp.presentation.util.HMILog
import com.gm.core.ModelInjector
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import io.reactivex.plugins.RxJavaPlugins
import javax.inject.Inject

class CleanArchBPApplication : Application(), HasAndroidInjector {

    /*
     * This object is needed to enable activities to know about the application dependencies
     */
    @Inject
    internal lateinit var androidDispatchingAndroidInjector: DispatchingAndroidInjector<Any>

    @Inject
    internal lateinit var cleanArchBPController: CleanArchBPController

    override fun onCreate() {
        super.onCreate()
        initializeInjector(ModelInjector.buildModelComponent(this))
        cleanArchBPController.startListeningToChronoUpdates()
        RxJavaPlugins.setErrorHandler { throwable -> HMILog.e("Uncaught failure: ", throwable) }
    }

    override fun androidInjector(): AndroidInjector<Any>? {
        return androidDispatchingAndroidInjector
    }

    /**
     * Creates the dagger application component and calls inject on the current application instance.
     * The inject operation will instantiate the @Injected annotated object.
     */
    private fun initializeInjector(modelComponent: ModelComponent) {
        DaggerPresentationComponent.factory().create(this, modelComponent).inject(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_ANY)
    fun onAny(source: LifecycleOwner, event: Lifecycle.Event) {
        if (HMILog.DEBUG) {
            HMILog.d(source.javaClass.simpleName + ":" + event.name)
        }
    }
}

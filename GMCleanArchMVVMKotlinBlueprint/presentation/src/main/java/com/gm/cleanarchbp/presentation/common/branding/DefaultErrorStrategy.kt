package com.gm.cleanarchbp.presentation.common.branding

import android.content.Context
import android.view.View
import androidx.annotation.StringRes
import androidx.appcompat.app.AlertDialog
import javax.inject.Inject

class DefaultErrorStrategy @Inject constructor() : ErrorStrategy {

    override fun showError(view: View, string: String) {
        showDialog(view.context, string)
    }

    override fun showError(view: View, @StringRes stringId: Int) {
        showError(view, view.context.getString(stringId))
    }

    private fun showDialog(context: Context, message: String) {
        val alertDialog: AlertDialog = AlertDialog.Builder(context).create()
        alertDialog.setTitle("Error")
        alertDialog.setMessage(message)
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK") { dialog, _ ->
            dialog.dismiss()
        }
        alertDialog.show()
    }
}

package com.gm.cleanarchbp.presentation.common.branding

import androidx.annotation.LayoutRes
import com.gm.cleanarchbp.R

enum class ScreenType(@LayoutRes val defaultLayoutId: Int) {
    MAIN(R.layout.activity_main)
}

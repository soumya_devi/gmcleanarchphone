/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */
package com.gm.cleanarchbp.presentation.common.view

import android.os.Bundle
import android.view.View

/**
 * This activity is responsible for making the inheriting activity a fullscreen activity.
 */
abstract class BaseFullScreenActivity : BaseActivity(), View.OnSystemUiVisibilityChangeListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.decorView.setOnSystemUiVisibilityChangeListener(this)
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) {
            hideNavigationBar()
        }
    }

    override fun onSystemUiVisibilityChange(visibility: Int) {
        if (FULL_SCREEN_FLAG_MASK and visibility != FULL_SCREEN_FLAG_MASK) {
            hideNavigationBar()
        }
    }

    protected fun hideNavigationBar() {
        window.decorView.systemUiVisibility = (View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                or View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                or View.SYSTEM_UI_FLAG_FULLSCREEN
                or View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY)
    }

    companion object {

        private const val FULL_SCREEN_FLAG_MASK =
                View.SYSTEM_UI_FLAG_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
    }
}

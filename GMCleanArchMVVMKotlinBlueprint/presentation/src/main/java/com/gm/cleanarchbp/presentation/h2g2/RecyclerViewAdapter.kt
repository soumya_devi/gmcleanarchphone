/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.presentation.h2g2

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.gm.cleanarchbp.R
import com.gm.cleanarchbp.presentation.RecyclerViewClickListener
import com.gm.cleanarchbp.presentation.viewmodel.h2g2.DataClass

class RecyclerViewAdapter(val data: ArrayList<DataClass>) : RecyclerView.Adapter<RecyclerViewAdapter.MyViewHolder>() {
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tabText.text = data[position].textView
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_tab, parent, false)
        return MyViewHolder(view)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tabText = itemView.findViewById<TextView>(R.id.tab_items)


    }
}
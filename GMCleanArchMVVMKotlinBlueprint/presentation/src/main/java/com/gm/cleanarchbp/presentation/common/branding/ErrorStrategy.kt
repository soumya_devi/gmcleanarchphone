package com.gm.cleanarchbp.presentation.common.branding

import android.view.View
import androidx.annotation.StringRes

interface ErrorStrategy {
    fun showError(view: View, string: String)

    fun showError(view: View, @StringRes stringId: Int)
}

/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.presentation

import com.gm.cleanarchbp.model.ModelComponent
import com.gm.core.annotation.PresentationScope

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

/**
 * A Dagger component whose lifetime is the life of the presentation application
 * This interface is annotated with component so that dagger can generate a fully-formed
 * dependency-injected implementation from a set of modules.
 * The generated implementation will be called Dagger{NameOfTheInterface} so in this case
 * DaggerPresentationComponent. This generated class is used in CleanArchBPApplication
 */
@PresentationScope
@Component(dependencies = [ModelComponent::class],
        modules = [
            AndroidSupportInjectionModule::class,
            ActivityInjectionModule::class,
            PresentationModule::class,
            ViewModelModule::class
        ]
)
internal interface PresentationComponent : AndroidInjector<CleanArchBPApplication> {

    /**
     * This makes sure that the component follows the contract defined in
     * [AndroidInjector.Factory], as this component inherits from AndroidInjector<T>.
     * In this case, to instantiate the generated component we will use
     * `DaggerPresentationComponent.factory().create(this, modelComponent` rather than
     * `DaggerPresentationComponent.builder().modelComponent(component).create(this)` rather than
     * `DaggerPresentationComponent.Builder()
     * .create(component)
     * .presentationModule(new PresentationModule(this)
     * .build() `
     * Provides an additional Builder method to be able to bind the model component dependency
    </T> */
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance application: CleanArchBPApplication,
                   modelComponent: ModelComponent): PresentationComponent
    }
}

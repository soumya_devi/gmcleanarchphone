/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.presentation.common.view


import android.content.Context
import androidx.fragment.app.Fragment
import dagger.android.support.AndroidSupportInjection

/**
 * Used as a base for every fragment. This could contain dependencies or view logic that will be
 * used in every fragment.
 * THIS IS NOT A UTIL CLASS. IT SHOULD NOT CONTAIN ANY LOGIC THAT COULD "POTENTIALLY BE USEFUL
 * FOR OTHER FRAGMENTS"
 */
abstract class BaseFragment : Fragment() {

    lateinit var baseActivity: BaseActivity

    override fun onAttach(context: Context) {
        /*
         * This provides every object annotated with @Inject
         */
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
        if (context is BaseActivity) {
            baseActivity = context
        } else {
            throw IllegalStateException("Every fragment has to be attached to BaseActivity")
        }
    }
}

/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.presentation.util

import android.util.Log
import com.gm.cleanarchbp.BuildConfig
import gm.util.log.ILogger
import gm.util.log.loggers.*
import java.io.PrintWriter
import java.io.StringWriter

object HMILog {

    val DEBUG: Boolean

    private val TAG = BuildConfig.APP_NAME + "_HMI"
    private val VERBOSE: Boolean
    private val INFO: Boolean
    private val WARNING: Boolean
    private val ERROR: Boolean

    private var sLogger: ILogger? = null

    init {
        var debug = true
        var verbose = false
        var info = false
        var warning = false
        var error = false

        try {
            verbose = Log.isLoggable(TAG, Log.VERBOSE)
            info = Log.isLoggable(TAG, Log.INFO)
            warning = Log.isLoggable(TAG, Log.WARN)
            error = Log.isLoggable(TAG, Log.ERROR)
        } catch (exception: Exception) {
            debug = false
        } finally {
            DEBUG = debug
            VERBOSE = verbose
            INFO = info
            WARNING = warning
            ERROR = error
        }

        if (VERBOSE || DEBUG) {
            initDebugLogger(AndroidLogger(TAG))
        } else {
            initDefaultLogger(AndroidLogger(TAG))
        }

        when {
            VERBOSE -> setLogLevel(ILogger.LogLevel.ALL)
            DEBUG -> setLogLevel(ILogger.LogLevel.DEBUG)
            INFO -> setLogLevel(ILogger.LogLevel.INFO)
            WARNING -> setLogLevel(ILogger.LogLevel.WARN)
            ERROR -> setLogLevel(ILogger.LogLevel.ERROR)
        }
    }

    /**
     * Set output logger for debug - output class/method name, line number and thread name.
     * Note that TraceLogger could impose a performance impact, it is only used in userdebug builds
     *
     * @param outputLogger The logger at the end of the decorator chain.
     */
    private fun initDebugLogger(outputLogger: ILogger) {
        sLogger = StoppableLogger(
                FormattingLogger(
                        TraceLogger(
                                ThreadLogger(outputLogger, "%2\$s\t(%1\$s)")
                        )
                )
        )

        // Setup the Defaults for the ILogger
        val traceLogger = getDecoratedLogger(TraceLogger::class.java) as TraceLogger?
        traceLogger?.addCallingClass(HMILog::class.java)
    }

    private fun setLogLevel(logLevel: ILogger.LogLevel) {
        val stoppableLogger = getDecoratedLogger(StoppableLogger::class.java) as StoppableLogger?
        if (stoppableLogger != null) {
            stoppableLogger.logLevel = logLevel
        }
    }

    /**
     * Extract the desired decorated Logger from the provided implementation of the [ILogger].
     *
     * @param decoratedLogger Desired decorator class.
     * @return The desired decorated Logger, or `null` if there is none.
     */
    private fun getDecoratedLogger(decoratedLogger: Class<*>?): ILogger? {
        var logger = sLogger
        if (decoratedLogger == null) {
            return null
        }
        while (logger != null) {
            if (logger.javaClass == decoratedLogger) {
                return logger
            }
            logger = logger.decorated
        }
        return null
    }

    /**
     * Set the output logger for general use
     *
     * @param outputLogger The logger at the end of the decorator chain.
     */
    private fun initDefaultLogger(outputLogger: ILogger) {
        sLogger = StoppableLogger(FormattingLogger(outputLogger))
    }

    /**
     * Log at Verbose level.
     *
     * @param message Message with %s replacements where the args should go, as per String.format() specification.
     * This avoids unnecessary string concatenation if no log is actually going to be made and makes the
     * log line look a little cleaner.
     * @param args    Arguments for the message string. There should be one %s in the message string for each argument.
     * If the last one is a Throwable then the stack trace is outputted and no %s in the format string
     * for it should be included.
     */
    fun v(message: String, vararg args: Any) {
        sLogger!!.v(message, *args)
    }

    /**
     * Log at Debug level.
     *
     * @param message Message with %s replacements where the args should go, as per String.format() specification.
     * This avoids unnecessary string concatenation if no log is actually going to be made and makes the
     * log line look a little cleaner.
     * @param args    Arguments for the message string. There should be one %s in the message string for each argument.
     * If the last one is a Throwable then the stack trace is outputted and no %s in the format string
     * for it should be included.
     */
    fun d(message: String, vararg args: Any) {
        if (DEBUG) {
            sLogger!!.d(message, *args)
        }
    }

    /**
     * Log at Info level.
     *
     * @param message Message with %s replacements where the args should go, as per String.format() specification.
     * This avoids unnecessary string concatenation if no log is actually going to be made and makes the
     * log line look a little cleaner.
     * @param args    Arguments for the message string. There should be one %s in the message string for each argument.
     * If the last one is a Throwable then the stack trace is outputted and no %s in the format string
     * for it should be included.
     */
    fun i(message: String, vararg args: Any) {
        sLogger!!.i(message, *args)
    }

    /**
     * Log at Warning level.
     *
     * @param message Message with %s replacements where the args should go, as per String.format() specification.
     * This avoids unnecessary string concatenation if no log is actually going to be made and makes the
     * log line look a little cleaner.
     * @param args    Arguments for the message string. There should be one %s in the message string for each argument.
     * If the last one is a Throwable then the stack trace is outputted and no %s in the format string
     * for it should be included.
     */
    fun w(message: String, vararg args: Any) {
        sLogger!!.w(message, *args)
    }

    /**
     * Log at Error level.
     *
     * @param message Message with %s replacements where the args should go, as per String.format() specification.
     * This avoids unnecessary string concatenation if no log is actually going to be made and makes the
     * log line look a little cleaner.
     * @param args    Arguments for the message string. There should be one %s in the message string for each argument.
     * If the last one is a Throwable then the stack trace is outputted and no %s in the format string
     * for it should be included.
     */
    fun e(message: String, vararg args: Any) {
        val messageAndError: String
        if (args.size > 0) {
            val sw = StringWriter()
            val pw = PrintWriter(sw)
            for (i in args.indices) {
                if (args[i] is Throwable) {
                    val error = args[i] as Throwable
                    error.printStackTrace(pw)
                }
            }
            messageAndError = message + "STACKTRACE: " + sw.toString()
        } else {
            messageAndError = message
        }
        sLogger!!.e(messageAndError, *args)
    }

    /**
     * Log a WTF.
     *
     * @param message Message with %s replacements where the args should go, as per String.format() specification.
     * This avoids unnecessary string concatenation if no log is actually going to be made and makes the
     * log line look a little cleaner.
     * @param args    Arguments for the message string. There should be one %s in the message string for each argument.
     * If the last one is a Throwable then the stack trace is outputted and no %s in the format string
     * for it should be included.
     */
    fun wtf(message: String, vararg args: Any) {
        sLogger!!.wtf(message, *args)
    }
}

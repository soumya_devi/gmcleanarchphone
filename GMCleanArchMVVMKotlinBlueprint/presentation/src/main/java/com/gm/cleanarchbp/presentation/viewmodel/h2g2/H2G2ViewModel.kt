/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */
package com.gm.cleanarchbp.presentation.viewmodel.chrono

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.gm.cleanarchbp.model.entity.PBContactsListDetails
import com.gm.cleanarchbp.model.usecase.FindTheAnswerToTheUniverseUseCase
import com.gm.cleanarchbp.model.usecase.model.AnswerFound
import com.gm.cleanarchbp.model.usecase.model.AnswerNotFound
import com.gm.cleanarchbp.model.usecase.model.FindSomeFeatureResult
import com.gm.cleanarchbp.presentation.common.viewmodel.BaseViewModel
import com.gm.cleanarchbp.presentation.common.viewmodel.ReloadableLiveData
import com.gm.cleanarchbp.presentation.common.viewmodel.UpdateIfChangedLiveData
import com.gm.cleanarchbp.presentation.util.HMILog
import com.gm.cleanarchbp.presentation.viewmodel.h2g2.H2G2TheAnswerUiData
import com.gm.core.viewmodel.OneShot
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject


/**
 * It tells the view what to map, show or hide. The presenter does not know directly if the view
 * is an activity or a fragment and it shall never be the case.
 * This class should have the minimum amount of android related things. If it has access to a
 * context, it shall only be to retrieve configuration values in res/xml/config.xml for example
 */
class H2G2ViewModel @Inject
constructor(
        private val findTheAnswerToTheUniverseUseCase: FindTheAnswerToTheUniverseUseCase)
    : BaseViewModel() {


   private val featureUiData= MutableLiveData<PBContactsListDetails>()



     fun findSomeFeature(): MutableLiveData<PBContactsListDetails> {
         featureUiData.value = findTheAnswerToTheUniverseUseCase.contactList()[0]
        return  featureUiData

    }

    override fun onCleared() {
        super.onCleared()
        findTheAnswerToTheUniverseUseCase.clear()
    }


}

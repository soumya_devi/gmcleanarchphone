/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.presentation.main

import com.gm.cleanarchbp.presentation.chrono.ChronoFragment
import com.gm.cleanarchbp.presentation.chrono.ChronoFragmentModule
import com.gm.cleanarchbp.presentation.common.BaseActivityModule
import com.gm.cleanarchbp.presentation.common.view.BaseActivity
import com.gm.cleanarchbp.presentation.h2g2.ContactsFragment
import com.gm.cleanarchbp.presentation.h2g2.ContactsFragmentModule
import com.gm.cleanarchbp.presentation.h2g2.H2G2Fragment
import com.gm.cleanarchbp.presentation.h2g2.H2G2FragmentModule
import com.gm.core.annotation.ActivityScope
import com.gm.core.annotation.FragmentScope
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [BaseActivityModule::class])
abstract class MainActivityModule {

    @Binds
    @ActivityScope
    internal abstract fun activityContext(mainActivity: MainActivity): BaseActivity

    @FragmentScope
    @ContributesAndroidInjector(modules = [H2G2FragmentModule::class])
    internal abstract fun h2g2FragmentModuleInjector(): H2G2Fragment


    @FragmentScope
    @ContributesAndroidInjector(modules = [ChronoFragmentModule::class])
    internal abstract fun chronoFragmentModuleInjector(): ChronoFragment

    @FragmentScope
    @ContributesAndroidInjector(modules = [ContactsFragmentModule::class])
    internal abstract fun contactsFragmentModuleInjector(): ContactsFragment
}

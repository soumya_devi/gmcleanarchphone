package com.gm.cleanarchbp.presentation

import android.view.View

interface RecyclerViewClickListener {
    fun recyclerViewListClicked( position: Int)
}
package com.gm.cleanarchbp.presentation.common.viewmodel

import androidx.lifecycle.MutableLiveData

class ReloadableLiveData<T>(
        private val doOnActive: () -> Unit,
        private val doOnInactive: () -> Unit = {}
) : MutableLiveData<T>() {
    override fun onActive() {
        doOnActive()
    }

    override fun onInactive() {
        doOnInactive
    }
}

package com.gm.cleanarchbp.presentation.viewmodel.chrono

sealed class ChronoUiData
data class ChronoEventUiData(val name: String, val value: Long) : ChronoUiData()
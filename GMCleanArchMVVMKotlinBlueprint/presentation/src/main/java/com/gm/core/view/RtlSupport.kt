/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.view

import android.annotation.SuppressLint
import android.app.Activity
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.Intent.ACTION_LOCALE_CHANGED
import android.content.IntentFilter
import android.text.TextUtils
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.gm.core.annotation.PresentationContext
import com.gm.core.annotation.PresentationScope
import java.util.*
import javax.inject.Inject

/**
 * Provides RTL support functionality per GM requirements.
 */
@PresentationScope
class RtlSupport @Inject
internal constructor(@PresentationContext context: Context) : BroadcastReceiver() {

    /**
     * Check if currently selected Locale is RTL.
     *
     * @return true if default text direction is RTL; LTR otherwise.
     */
    var isRtlLocaleSelected: Boolean = false
        private set

    init {
        val filter = IntentFilter(ACTION_LOCALE_CHANGED)
        context.registerReceiver(this, filter)
        refreshLocale()
    }

    private fun refreshLocale() {
        isRtlLocaleSelected = TextUtils.getLayoutDirectionFromLocale(Locale.getDefault()) == View.LAYOUT_DIRECTION_RTL
    }

    /**
     * Adjust layout's text elements for RTL.  Activity's layout graph is scanned from root for
     * any occurrences of text elements; left-aligned text is changed to right-aligned if necessary
     * (RTL locale selected).
     *
     * @param activity Activity which layout has to be processed.
     */
    fun adjustForRtl(activity: Activity) {
        if (isRtlLocaleSelected) {
            val root = activity.window.decorView
                    .findViewById<View>(android.R.id.content)
            adjustGroupForRtl(root)
        }
    }

    /**
     * Adjust layout's text elements for RTL.  Layout graph is scanned for any occurrences
     * of text elements; left-aligned text is changed to right-aligned if necessary
     * (RTL locale selected).
     *
     * @param root Layout root element to be processed.
     */
    fun adjustForRtl(root: View) {
        if (isRtlLocaleSelected) {
            adjustGroupForRtl(root)
        }
    }

    @SuppressLint("RtlHardcoded")
    private fun adjustGroupForRtl(root: View) {
        if (root is TextView) {
            val g = root.gravity
            if (g and Gravity.LEFT > 0) {
                root.gravity = Gravity.RIGHT
            }
        } else if (root is ViewGroup) {
            for (idx in root.childCount - 1 downTo 0) {
                adjustGroupForRtl(root.getChildAt(idx))
            }
        }
    }

    override fun onReceive(context: Context, intent: Intent) {
        refreshLocale()
    }
}

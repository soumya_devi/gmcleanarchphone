/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.annotation


import javax.inject.Qualifier

/**
 * Qualifier allowing dagger to make a difference between the activity context
 * and the presentation (application) context
 * It server the same behavior as using @Named("activity.context")
 */
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class PresentationContext

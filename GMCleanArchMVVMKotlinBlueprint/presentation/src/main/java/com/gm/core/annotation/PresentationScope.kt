/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.annotation

import javax.inject.Scope

/**
 * Scope relative to the PresentationComponent. Every object defined under this scope shall be used
 * inside the presentation module.
 */
@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class PresentationScope

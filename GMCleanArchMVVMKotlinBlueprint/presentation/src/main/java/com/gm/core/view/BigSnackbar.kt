/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */
package com.gm.core.view

import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.snackbar.SnackbarContentLayout

object BigSnackbar {
    fun make(view: View,
             text: CharSequence,
             duration: Int,
             @StringRes actionTextResId: Int,
             listener: View.OnClickListener?): Snackbar {
        val snackbar: Snackbar = make(view, text, duration)
        snackbar.setAction(actionTextResId, listener)
        return snackbar
    }

    fun make(view: View,
             text: CharSequence,
             duration: Int): Snackbar {
        val context = view.context
        val snackbar: Snackbar = Snackbar.make(view, text, duration)
        val snackBarLayout: Snackbar.SnackbarLayout = snackbar.getView() as Snackbar.SnackbarLayout
        val layoutParams: ViewGroup.LayoutParams = snackBarLayout.getLayoutParams()
        layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        snackBarLayout.setLayoutParams(layoutParams)
        val contentLayout: SnackbarContentLayout = snackBarLayout.getChildAt(0) as SnackbarContentLayout
        val contentLayoutParams: ViewGroup.LayoutParams = contentLayout.getLayoutParams()
        contentLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT
        contentLayout.setLayoutParams(contentLayoutParams)
        val textView: TextView = contentLayout.findViewById(com.google.android.material.R.id.snackbar_text)
        textView.textSize = 50f
        textView.setTextColor(ContextCompat.getColor(context, android.R.color.holo_blue_dark))

        val button: Button = contentLayout.findViewById(com.google.android.material.R.id.snackbar_action)
        button.textSize = 50f
        button.setTextColor(ContextCompat.getColor(context, android.R.color.holo_blue_dark))
        return snackbar
    }
}



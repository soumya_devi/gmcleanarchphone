/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.annotation

import javax.inject.Scope

/**
 * A scoping annotation to permit objects whose lifetime should
 * conform to the life of the activity to be memorized in the
 * correct component.
 */
@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope

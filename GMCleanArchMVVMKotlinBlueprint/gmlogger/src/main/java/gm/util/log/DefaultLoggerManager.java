/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package gm.util.log;

import android.util.Log;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import gm.util.log.loggers.AndroidLogger;
import gm.util.log.loggers.FormattingLogger;
import gm.util.log.loggers.StoppableLogger;
import gm.util.log.loggers.SystemOutLogger;
import gm.util.log.loggers.ThreadLogger;
import gm.util.log.loggers.TraceLogger;

/**
 * Generic Logger Manager. This logger allows the use of additional logging functionality through
 * a wrapped API. This class is intended to be the centralized point for logging. It supports
 * using a the Android Logger and a Decorated Logger Chain.
 * <br><br>
 * Android Util Methods are identical to {@link Log}. The only difference is they can
 * optionally filter the Verbose messages independent of {@link Log}. These log lines
 * will resemble:<br>
 * D/YourTag: Your Log Message
 * <br><br>
 * The {@link ILogger} approach uses a decorator pattern to manipulate the Log message. All the logs will
 * be tagged as tag messages but will contain class+log line + thread:<br>
 * D/<YourTag>: MainActivity.onCreate(19)  Your Log Message             (main)
 * <br><br>
 * This version is indented to be 'subclassed' and then configured / extended as appropriate.
 * <br><br>
 * Your subclass should resemble:
 * <pre><code>
 *     class MyLogger extends DefaultLoggerManager {
 *
 *         public static void init() {
 *             initDefaultLoggerChain("<your tag>");
 *         }
 *     }
 * </code></pre>
 * <br><br>
 * You then need to ensure you call 'init()' prior to logging any messages, or the {@link #DEFAULT_TAG} tag will
 * be used.
 *
 * @hide
 */
public class DefaultLoggerManager {

    /**
     * Default tag value.
     */
    private static final String DEFAULT_TAG = "default";

    /**
     * Logger itself.
     */
    protected static ILogger sLogger;

    static {
        initDefaultLoggerChain(null);
    }

    /**
     * Default constructor.
     */
    public DefaultLoggerManager() {
        super();
    }

    /**
     * Will be called in Static Initializer of DefaultLoggerManager.<br>
     * Should be used to initialize the Logging Chain.
     *
     * @param tag The Tag to optionally use for log messages.
     */
    protected static void initDefaultLoggerChain(String tag) {
        if (sLogger != null) {
            return;
        }
        if (tag == null) {
            tag = DEFAULT_TAG;
        }
        if (isUnderTest()) {
            setOutputLogger(new SystemOutLogger());
        } else {
            setOutputLogger(new AndroidLogger(tag));
        }
    }

    /**
     * Checks if currently under test or not. This currently will check to see if there is
     * a org.junit class in the stack trace.<br>
     *
     * @return {@code true} if running under a local unit test, {@code false} otherwise.
     */
    public static boolean isUnderTest() {
        final StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();
        final List<StackTraceElement> list = Arrays.asList(stackTrace);
        for (final StackTraceElement element : list) {
            if (element.getClassName().startsWith("org.junit.")) {
                return true;
            }
        }
        return false;
    }

    /**
     * Will attempt to set the {@link ILogger.LogLevel}. This will only work if the Stoppable decorator is being
     * used.
     *
     * @param logLevel Max {@link ILogger.LogLevel} to use.
     * @return {@code true} if stoppable logger was found and set successfully, {@code false} otherwise.
     */
    public static boolean setLogLevel(final ILogger.LogLevel logLevel) {
        final StoppableLogger stoppableLogger = (StoppableLogger) getDecoratedLogger(StoppableLogger.class);
        if (stoppableLogger != null) {
            stoppableLogger.setLogLevel(logLevel);
            return true;
        }
        return false;
    }

    /**
     * Returns the current {@link ILogger.LogLevel}.
     *
     * @return Current {@link ILogger.LogLevel}.
     */
    public static ILogger.LogLevel getLogLevel() {
        StoppableLogger stoppableLogger = (StoppableLogger) getDecoratedLogger(StoppableLogger.class);
        if (stoppableLogger == null) {
            return ILogger.LogLevel.ALL;
        }
        return stoppableLogger.getLogLevel();
    }

    /**
     * Log at Verbose level.
     *
     * @param message Message with %s replacements where the args should go, as per String.format() specification.
     *                This avoids unnecessary string concatenation if no log is actually going to be made and makes the
     *                log line look a little cleaner.
     * @param args    Arguments for the message string. There should be one %s in the message string for each argument.
     *                If the last one is a Throwable then the stack trace is outputted and no %s in the format string
     *                for it should be included.
     */
    public static void v(String message, Object... args) {
        getSLogger().v(message, args);
    }

    /**
     * Log at Debug level.
     *
     * @param message Message with %s replacements where the args should go, as per String.format() specification.
     *                This avoids unnecessary string concatenation if no log is actually going to be made and makes the
     *                log line look a little cleaner.
     * @param args    Arguments for the message string. There should be one %s in the message string for each argument.
     *                If the last one is a Throwable then the stack trace is outputted and no %s in the format string
     *                for it should be included.
     */
    public static void d(String message, Object... args) {
        getSLogger().d(message, args);
    }

    /**
     * Log at Info level.
     *
     * @param message Message with %s replacements where the args should go, as per String.format() specification.
     *                This avoids unnecessary string concatenation if no log is actually going to be made and makes the
     *                log line look a little cleaner.
     * @param args    Arguments for the message string. There should be one %s in the message string for each argument.
     *                If the last one is a Throwable then the stack trace is outputted and no %s in the format string
     *                for it should be included.
     */
    public static void i(String message, Object... args) {
        getSLogger().i(message, args);
    }

    /**
     * Log at Warning level.
     *
     * @param message Message with %s replacements where the args should go, as per String.format() specification.
     *                This avoids unnecessary string concatenation if no log is actually going to be made and makes the
     *                log line look a little cleaner.
     * @param args    Arguments for the message string. There should be one %s in the message string for each argument.
     *                If the last one is a Throwable then the stack trace is outputted and no %s in the format string
     *                for it should be included.
     */
    public static void w(String message, Object... args) {
        getSLogger().w(message, args);
    }

    /**
     * Log at Error level.
     *
     * @param message Message with %s replacements where the args should go, as per String.format() specification.
     *                This avoids unnecessary string concatenation if no log is actually going to be made and makes the
     *                log line look a little cleaner.
     * @param args    Arguments for the message string. There should be one %s in the message string for each argument.
     *                If the last one is a Throwable then the stack trace is outputted and no %s in the format string
     *                for it should be included.
     */
    public static void e(String message, Object... args) {
        getSLogger().e(message, args);
    }

    /**
     * Log a WTF.
     *
     * @param message Message with %s replacements where the args should go, as per String.format() specification.
     *                This avoids unnecessary string concatenation if no log is actually going to be made and makes the
     *                log line look a little cleaner.
     * @param args    Arguments for the message string. There should be one %s in the message string for each argument.
     *                If the last one is a Throwable then the stack trace is outputted and no %s in the format string
     *                for it should be included.
     */
    public static void wtf(String message, Object... args) {
        getSLogger().wtf(message, args);
    }

    /**
     * Log at the custom level with custom properties. Each Logger decorator could have its own
     * properties.<br>
     * NOTE: Regularly you would not call this directly, instead a subclass of DefaultLoggerManager would
     * call this for your and provide a custom log level.
     *
     * @param message Message to log.
     * @param args    Arguments to provide with the message.
     */
    public static void custom(Map<String, Object> properties, String message, Object... args) {
        getSLogger().custom(properties, message, args);
    }

    /**
     * Returns the SLogger for this class.
     * Today a process will share the same logger. This method would allow us to technically change
     * that and use a SLogger registered to the classes name if required.
     *
     * @return SLogger to use for logging.
     */
    protected static ILogger getSLogger() {
        return sLogger;
    }

    /**
     * Extract the desired decorated Logger from the provided implementation of the {@link ILogger}.
     *
     * @param decoratedLogger Desired decorator class.
     * @return The desired decorated Logger, or {@code null} if there is none.
     */
    protected static ILogger getDecoratedLogger(final Class decoratedLogger) {
        ILogger result = null;
        ILogger logger = sLogger;
        if (decoratedLogger == null) {
            return result;
        }
        while (logger != null) {
            if (logger.getClass() == decoratedLogger) {
                result = logger;
                break;
            }
            logger = logger.getDecorated();
        }
        return result;
    }

    /**
     * Will determine where to direct last message to.<br>
     * Typically will be the {@link AndroidLogger}. Can be {@link SystemOutLogger}.<br>
     * By default, this will then load the default logger chain which is:
     * - {@link StoppableLogger}, {@link FormattingLogger}, {@link TraceLogger}, {@link ThreadLogger}
     *
     * @param outputLogger The Logger to set as the System output logger.
     */
    public static void setOutputLogger(ILogger outputLogger) {
        sLogger = new StoppableLogger(
                new FormattingLogger(
                        new TraceLogger(
                                new ThreadLogger(outputLogger, "%2$s\t(%1$s)")
                        )
                )
        );

        // Setup the Defaults for the ILogger
        final TraceLogger traceLogger = (TraceLogger) getDecoratedLogger(TraceLogger.class);
        if (traceLogger != null) {
            traceLogger.addCallingClass(DefaultLoggerManager.class);
        }
    }

    /**
     * Wrappers around {@link Log} to continue to use Tags.
     */
    public static class Tag {

        /**
         * Default constructor.
         */
        private Tag() {
            super();
        }

        /**
         * Send a {@link Log#DEBUG} log message.
         *
         * @param tag Used to identify the source of a log message.  It usually identifies
         *            the class or activity where the log call occurs.
         * @param msg The message you would like logged.
         */
        public static void d(final String tag, final String msg) {
            d(tag, msg, null);
        }

        /**
         * Send a {@link Log#DEBUG} log message and log the exception.
         *
         * @param tag Used to identify the source of a log message.  It usually identifies
         *            the class or activity where the log call occurs.
         * @param msg The message you would like logged.
         * @param tr  An exception to log
         */
        public static void d(final String tag, final String msg, final Throwable tr) {
            custom(ILogger.LogLevel.DEBUG, tag, msg, tr);
        }

        /**
         * Send a {@link Log#ERROR} log message.
         *
         * @param tag Used to identify the source of a log message.  It usually identifies
         *            the class or activity where the log call occurs.
         * @param msg The message you would like logged.
         */
        public static void e(final String tag, final String msg) {
            e(tag, msg, null);
        }

        /**
         * Send a {@link Log#ERROR} log message and log the exception.
         *
         * @param tag Used to identify the source of a log message.  It usually identifies
         *            the class or activity where the log call occurs.
         * @param msg The message you would like logged.
         * @param tr  An exception to log
         */
        public static void e(final String tag, final String msg, final Throwable tr) {
            custom(ILogger.LogLevel.ERROR, tag, msg, tr);
        }

        /**
         * Send a {@link Log#WARN} log message.
         *
         * @param tag Used to identify the source of a log message.  It usually identifies
         *            the class or activity where the log call occurs.
         * @param msg The message you would like logged.
         */
        public static void w(final String tag, final String msg) {
            w(tag, msg, null);
        }

        /**
         * Send a {@link Log#WARN} log message and log the exception.
         *
         * @param tag Used to identify the source of a log message.  It usually identifies
         *            the class or activity where the log call occurs.
         * @param msg The message you would like logged.
         * @param tr  An exception to log
         */
        public static void w(final String tag, final String msg, final Throwable tr) {
            custom(ILogger.LogLevel.WARN, tag, msg, tr);
        }

        /**
         * Send a {@link Log#INFO} log message.
         *
         * @param tag Used to identify the source of a log message.  It usually identifies
         *            the class or activity where the log call occurs.
         * @param msg The message you would like logged.
         */
        public static void i(final String tag, final String msg) {
            i(tag, msg, null);
        }

        /**
         * Send a {@link Log#INFO} log message and log the exception.
         *
         * @param tag Used to identify the source of a log message.  It usually identifies
         *            the class or activity where the log call occurs.
         * @param msg The message you would like logged.
         * @param tr  An exception to log
         */
        public static void i(final String tag, final String msg, final Throwable tr) {
            custom(ILogger.LogLevel.INFO, tag, msg, tr);
        }

        /**
         * Send a {@link Log#VERBOSE} log message.
         *
         * @param tag Used to identify the source of a log message.  It usually identifies
         *            the class or activity where the log call occurs.
         * @param msg The message you would like logged.
         */
        public static void v(final String tag, final String msg) {
            v(tag, msg, null);
        }

        /**
         * Send a {@link Log#VERBOSE} log message and log the exception.
         *
         * @param tag Used to identify the source of a log message.  It usually identifies
         *            the class or activity where the log call occurs.
         * @param msg The message you would like logged.
         * @param tr  An exception to log
         */
        public static void v(final String tag, final String msg, final Throwable tr) {
            custom(ILogger.LogLevel.VERBOSE, tag, msg, tr);
        }

        /**
         * Log a WTF
         *
         * @param tag Used to identify the source of a log message.  It usually identifies
         *            the class or activity where the log call occurs.
         * @param msg The message you would like logged.
         * @param tr  An exception to log
         */
        public static void wtf(final String tag, final String msg, final Throwable tr) {
            custom(ILogger.LogLevel.ASSERT, tag, msg, tr);
        }

        /**
         * Will utilize the 'custom' logger to setup through the logger chain.
         *
         * @param level The Log level.
         * @param tag   The Log tag.
         * @param msg   The Log message.
         * @param tr    Optional throwable.
         */
        public static void custom(final ILogger.LogLevel level, final String tag, final String msg, final Throwable tr) {
            final Map<String, Object> map = new HashMap<String, Object>();
            map.put(ILogger.CUSTOM_LOG_LEVEL, level);
            map.put(ILogger.CUSTOM_LOG_TAG, tag);
            getSLogger().custom(map, msg, tr);
        }

    }

}

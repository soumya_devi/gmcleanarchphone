/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package gm.util.log.internal;

import java.util.Map;

import gm.util.log.DefaultLoggerManager;
import gm.util.log.loggers.AndroidLogger;
import gm.util.log.loggers.SystemOutLogger;
import gm.util.log.loggers.TraceLogger;

/**
 * Logger for the Info3 package internal usage. By default these will use the process logger if it
 * exists.
 *
 * @hide
 */
public class Info3InternalLogger extends DefaultLoggerManager {

    static {
        if (getSLogger() == null) {
            if (isUnderTest()) {
                setOutputLogger(new SystemOutLogger());
            } else {
                setOutputLogger(new AndroidLogger("info3_internal"));
            }
            final TraceLogger traceLogger = (TraceLogger) getDecoratedLogger(TraceLogger.class);
            if (traceLogger != null) {
                traceLogger.addCallingClass(Info3InternalLogger.class);
            }
        }
    }

    /**
     * Default constructor.
     */
    public Info3InternalLogger() {
        super();
    }

    public static void v(final String message, final Object... args) {
        DefaultLoggerManager.v(message, args);
    }

    public static void d(final String message, final Object... args) {
        DefaultLoggerManager.d(message, args);
    }

    public static void i(final String message, final Object... args) {
        DefaultLoggerManager.i(message, args);
    }

    public static void w(final String message, final Object... args) {
        DefaultLoggerManager.w(message, args);
    }

    public static void e(final String message, final Object... args) {
        DefaultLoggerManager.e(message, args);
    }

    public static void wtf(final String message, final Object... args) {
        DefaultLoggerManager.wtf(message, args);
    }

    public static void custom(final Map<String, Object> properties, final String message, final Object... args) {
        DefaultLoggerManager.custom(properties, message, args);
    }
}

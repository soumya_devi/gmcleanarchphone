/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package gm.util.log.loggers;

import java.util.Map;

import gm.util.log.ILogger;

/**
 * Implementation of the {@link ILogger} designed to decorate message by adding thread name to the end of the log line.
 *
 * @hide
 */
public class ThreadLogger implements ILogger {

    /**
     * {@link ILogger} to log messages to.
     */
    private final ILogger mLogger;

    /**
     * String to apply format to.
     */
    private final String mFormatString;

    /**
     * Main constructor.
     *
     * @param logger       {@link ILogger} to send formatted messages to. No further arguments are sent.
     * @param formatString String for String.format where the first parameter is the thread name and the second
     *                     parameter is the message. For example to put the thread name in brackets after the message
     *                     use "%2$s (%1$s)"
     */
    public ThreadLogger(final ILogger logger, final String formatString) {
        super();
        mLogger = logger;
        mFormatString = formatString;
    }

    @Override
    public void v(final String message, final Object... args) {
        mLogger.v(format(message), args);
    }

    @Override
    public void d(final String message, final Object... args) {
        mLogger.d(format(message), args);
    }

    @Override
    public void i(final String message, final Object... args) {
        mLogger.i(format(message), args);
    }

    @Override
    public void w(final String message, final Object... args) {
        mLogger.w(format(message), args);
    }

    @Override
    public void e(final String message, final Object... args) {
        mLogger.e(format(message), args);
    }

    @Override
    public void wtf(final String message, final Object... args) {
        mLogger.wtf(format(message), args);
    }

    @Override
    public void custom(final Map<String, Object> properties, final String message, final Object... args) {
        mLogger.custom(properties, format(message), args);
    }

    @Override
    public ILogger getDecorated() {
        return mLogger;
    }

    /**
     * Format provided message (attach current thread name to it).
     *
     * @param message Message to format.
     * @return Formatted message.
     */
    private String format(final String message) {
        return String.format(mFormatString, Thread.currentThread().getName(), message);
    }
}

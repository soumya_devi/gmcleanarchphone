/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package gm.util.log.loggers;

import android.util.Log;

import java.util.Map;

import gm.util.log.ILogger;

/**
 * Implementation of the {@link ILogger} designed to use Android's {@link Log}.<br>
 * NOTE: For custom logger - default level is 'verbose'.
 *
 * @hide
 */
public class AndroidLogger extends AbstractILogger {

    /**
     * Log tag to use for all log messages.
     */
    private final String mTag;

    /**
     * Constructor.
     *
     * @param tag    Log tag to use for all log messages.
     * @param logger Optional logger that will be called post-output.
     */
    public AndroidLogger(final String tag, final ILogger logger) {
        super(logger);
        this.mTag = tag;
    }

    /**
     * Constructor.
     *
     * @param tag Log tag to use for all log messages.
     */
    public AndroidLogger(final String tag) {
        this(tag, null);
    }

    @Override
    protected void logv(final String message, final Object... args) {
        Log.v(mTag, message);
    }

    @Override
    protected void logd(final String message, final Object... args) {
        Log.d(mTag, message);
    }

    @Override
    protected void logi(final String message, final Object... args) {
        Log.i(mTag, message);
    }

    @Override
    protected void logw(final String message, final Object... args) {
        Log.w(mTag, message);
    }

    @Override
    protected void loge(final String message, final Object... args) {
        Log.e(mTag, message);
    }

    @Override
    protected void logwtf(final String message, final Object... args) {
        Log.wtf(mTag, message);
    }

    @Override
    protected void logcustom(final Map<String, Object> properties, final String message, final Object... args) {
        LogLevel level = (LogLevel) properties.get(CUSTOM_LOG_LEVEL);
        if (level == null) {
            level = LogLevel.VERBOSE;
        }
        String logTag = (String) properties.get(CUSTOM_LOG_TAG);
        if (logTag == null) {
            logTag = mTag;
        }

        switch (level) {
            case VERBOSE:
                Log.v(logTag, message);
                break;
            case DEBUG:
                Log.d(logTag, message);
                break;
            case INFO:
                Log.i(logTag, message);
                break;
            case WARN:
                Log.w(logTag, message);
                break;
            case ERROR:
                Log.e(logTag, message);
                break;
            case ASSERT:
                Log.wtf(logTag, message);
                break;
            default:
                break;
        }
    }
}

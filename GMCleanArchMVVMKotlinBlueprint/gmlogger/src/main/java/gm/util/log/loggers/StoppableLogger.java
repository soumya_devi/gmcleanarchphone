/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package gm.util.log.loggers;

import java.util.Map;

import gm.util.log.ILogger;

/**
 * Implementation of the {@link ILogger} designed in a way to log only if its log level is high enough.
 *
 * @hide
 */
public class StoppableLogger implements ILogger {

    /**
     * {@link ILogger} to log messages to.
     */
    private final ILogger mLogger;

    /**
     * @see #setLogLevel(LogLevel)
     */
    private LogLevel mLogLevel = LogLevel.ALL;

    /**
     * Main constructor.
     *
     * @param logger {@link ILogger} to log to if the level is high enough.
     */
    public StoppableLogger(final ILogger logger) {
        super();
        mLogger = logger;
    }

    /**
     * @see #setLogLevel(LogLevel)
     */
    public LogLevel getLogLevel() {
        return mLogLevel;
    }

    /**
     * Set the log level to log. Ordinal is used to determine if should log.<br>
     * E.g. Log Level of DEBUG would allow DEBUG, INFO, WARN, ERROR.
     *
     * @param logLevel Min level to log. Default is ALL.
     */
    public void setLogLevel(final LogLevel logLevel) {
        mLogLevel = logLevel;
    }

    @Override
    public void v(final String message, final Object... args) {
        if (LogLevel.VERBOSE.getLevel() >= mLogLevel.getLevel()) {
            mLogger.v(message, args);
        }
    }

    @Override
    public void d(final String message, final Object... args) {
        if (LogLevel.DEBUG.getLevel() >= mLogLevel.getLevel()) {
            mLogger.d(message, args);
        }
    }

    @Override
    public void i(final String message, final Object... args) {
        if (LogLevel.INFO.getLevel() >= mLogLevel.getLevel()) {
            mLogger.i(message, args);
        }
    }

    @Override
    public void w(final String message, final Object... args) {
        if (LogLevel.WARN.getLevel() >= mLogLevel.getLevel()) {
            mLogger.w(message, args);
        }
    }

    @Override
    public void e(final String message, final Object... args) {
        if (LogLevel.ERROR.getLevel() >= mLogLevel.getLevel()) {
            mLogger.e(message, args);
        }
    }

    @Override
    public void wtf(final String message, final Object... args) {
        mLogger.wtf(message, args);
    }

    @Override
    public void custom(final Map<String, Object> properties, final String message, final Object... args) {
        LogLevel level = null;
        if (properties != null) {
            level = (LogLevel) properties.get(ILogger.CUSTOM_LOG_LEVEL);
        }
        if (level == null || level.getLevel() >= mLogLevel.getLevel()) {
            mLogger.custom(properties, message, args);
        }
    }

    @Override
    public ILogger getDecorated() {
        return mLogger;
    }
}

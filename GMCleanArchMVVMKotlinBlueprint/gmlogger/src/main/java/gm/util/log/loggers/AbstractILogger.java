/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package gm.util.log.loggers;

import java.util.Map;

import gm.util.log.ILogger;

/**
 * Abstraction over {@link ILogger} interface.
 *
 * @hide
 */
public abstract class AbstractILogger implements ILogger {

    /**
     * Logger implementation.
     */
    private final ILogger mLogger;

    /**
     * Main constructor.
     *
     * @param logger Logger implementation.
     */
    public AbstractILogger(final ILogger logger) {
        super();
        mLogger = logger;
    }

    @Override
    public void v(final String message, final Object... args) {
        if (mLogger != null) {
            mLogger.v(message, args);
        }
        logv(message, args);
    }

    @Override
    public void d(final String message, final Object... args) {
        if (mLogger != null) {
            mLogger.d(message, args);
        }
        logd(message, args);
    }

    @Override
    public void i(final String message, final Object... args) {
        if (mLogger != null) {
            mLogger.i(message, args);
        }
        logi(message, args);
    }

    @Override
    public void w(final String message, final Object... args) {
        if (mLogger != null) {
            mLogger.w(message, args);
        }
        logw(message, args);
    }

    @Override
    public void e(final String message, final Object... args) {
        if (mLogger != null) {
            mLogger.e(message, args);
        }
        loge(message, args);
    }

    @Override
    public void wtf(final String message, final Object... args) {
        if (mLogger != null) {
            mLogger.wtf(message, args);
        }
        logwtf(message, args);
    }

    @Override
    public void custom(final Map<String, Object> properties, final String message, final Object... args) {
        if (mLogger != null) {
            mLogger.custom(properties, message, args);
        }
        logcustom(properties, message, args);
    }

    @Override
    public ILogger getDecorated() {
        return mLogger;
    }

    protected abstract void logv(final String message, final Object... args);

    protected abstract void logd(final String message, final Object... args);

    protected abstract void logi(final String message, final Object... args);

    protected abstract void logw(final String message, final Object... args);

    protected abstract void loge(final String message, final Object... args);

    protected abstract void logwtf(final String message, final Object... args);

    protected abstract void logcustom(final Map<String, Object> properties, final String message, final Object... args);
}

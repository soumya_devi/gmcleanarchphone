/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package gm.util.log.loggers;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import gm.util.log.ILogger;

/**
 * Implementation of the {@link ILogger} designed to decorates message by adding file and line number
 * for easier debugging, e.g. ClassName.YourMethod(11).<br>
 * Dev Note: Negative line number means that the line number could not be found.<br>
 * Note: Performance impact in measurements has been negligible. If performance becomes concern
 * can limit this to verbose / debug messages only.
 *
 * @hide
 */
public class TraceLogger implements ILogger {

    /**
     * {@link ILogger} to log messages to.
     */
    private final ILogger mLogger;

    /**
     * Set of class names implemented {@link ILogger} interface.
     */
    private final Set<String> mCallingClasses = new HashSet<String>();

    /**
     * Main constructor.
     *
     * @param logger {@link ILogger} to log messages to.
     */
    public TraceLogger(final ILogger logger) {
        // Add known ILogger classes.
        mCallingClasses.add(FormattingLogger.class.getName());
        mCallingClasses.add(ThreadLogger.class.getName());
        mCallingClasses.add(TraceLogger.class.getName());
        mCallingClasses.add(StoppableLogger.class.getName());
        mLogger = logger;
    }

    @Override
    public void v(final String message, final Object... args) {
        mLogger.v(calcStackLine() + message, args);
    }

    @Override
    public void d(final String message, final Object... args) {
        mLogger.d(calcStackLine() + message, args);
    }

    @Override
    public void i(final String message, final Object... args) {
        mLogger.i(calcStackLine() + message, args);
    }

    @Override
    public void w(final String message, final Object... args) {
        mLogger.w(calcStackLine() + message, args);
    }

    @Override
    public void e(final String message, final Object... args) {
        mLogger.e(calcStackLine() + message, args);
    }

    @Override
    public void wtf(final String message, final Object... args) {
        mLogger.wtf(calcStackLine() + message, args);
    }

    @Override
    public void custom(final Map<String, Object> properties, final String message, final Object... args) {
        mLogger.custom(properties, calcStackLine() + message, args);
    }

    @Override
    public ILogger getDecorated() {
        return mLogger;
    }

    /**
     * Adds a class to be skipped when displaying who the caller was.
     */
    public void addCallingClass(final Class<?> callingClass) {
        mCallingClasses.add(callingClass.getName());
    }

    /**
     * Creates stacktrace line of the current execution point.
     *
     * @return Stacktrace line.
     */
    private String calcStackLine() {
        final StackTraceElement[] stack = new Exception().getStackTrace();
        // Depth of 2 to ignore this class + ILogger
        int depth = 2;
        for (int i = depth; i < stack.length; i++) {
            if (mCallingClasses.contains(stack[i].getClassName())) {
                depth = i + 1;
            }
        }
        if (depth >= stack.length) {
            return "";
        }
        final StackTraceElement line = stack[depth];
        String desc = line.getClassName();
        int period = desc.lastIndexOf(".");
        if (period != -1) {
            desc = desc.substring(period + 1);
        }
        desc += "." + line.getMethodName();
        final int lineNumber = line.getLineNumber();
        if (lineNumber >= 0) {
            desc += "(" + lineNumber + ")";
        }
        desc += "\t";
        return desc;
    }
}

/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package gm.util.log.loggers;

import android.os.Build;
import android.util.Log;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Map;

import gm.util.log.ILogger;

/**
 * Implementation of the {@link ILogger} designed to format the message string with the arguments
 * using {@code String.format()}.
 *
 * @hide
 */
public class FormattingLogger implements ILogger {

    /**
     * {@link ILogger} interface.
     */
    private final ILogger mLogger;

    /**
     * Main constructor.
     *
     * @param logger {@link ILogger} implementation to send formatted messages to.
     */
    public FormattingLogger(final ILogger logger) {
        super();
        mLogger = logger;
    }

    /**
     * Util function to get a loggable stack trace from a {@link Throwable}.
     *
     * @param throwable An exception to log.
     */
    private static String getStackTraceString(final Throwable throwable) {
        if (Build.HARDWARE != null) {
            // Android get Stack Track String is much quicker
            return Log.getStackTraceString(throwable);
        }

        if (throwable == null) {
            return "";
        }

        final StringWriter stringWriter = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(stringWriter);
        throwable.printStackTrace(printWriter);
        printWriter.flush();
        return stringWriter.toString();
    }

    @Override
    public void v(final String message, final Object... args) {
        mLogger.v(formatMessage(message, args));
    }

    @Override
    public void d(final String message, final Object... args) {
        mLogger.d(formatMessage(message, args));
    }

    @Override
    public void i(final String message, final Object... args) {
        mLogger.i(formatMessage(message, args));
    }

    @Override
    public void w(final String message, final Object... args) {
        mLogger.w(formatMessage(message, args));
    }

    @Override
    public void e(final String message, final Object... args) {
        mLogger.e(formatMessage(message, args));
    }

    @Override
    public void wtf(final String message, final Object... args) {
        mLogger.wtf(formatMessage(message, args));
    }

    @Override
    public void custom(final Map<String, Object> properties, final String message, final Object... args) {
        mLogger.custom(properties, formatMessage(message, args));
    }

    @Override
    public ILogger getDecorated() {
        return mLogger;
    }

    /**
     * Formats provided message, if the last argument is {@link Throwable} appends stacktrace to message.
     *
     * @param message Message to format.
     * @param args    Input arguments to apply to message.
     * @return Formatted message.
     */
    private String formatMessage(final String message, final Object... args) {
        if (args != null && args.length > 0 && args[args.length - 1] instanceof Throwable) {
            final Object[] truncated = Arrays.copyOf(args, args.length - 1);
            final Throwable ex = (Throwable) args[args.length - 1];
            return formatArgs(message, truncated) + '\n' + ex.getMessage() + '\n' + getStackTraceString(ex);
        }
        return formatArgs(message, args);
    }

    /**
     * Formats provided message with arguments.
     *
     * @param message Message to format.
     * @param args    Input arguments to apply to message.
     * @return Formatted message.
     */
    private String formatArgs(final String message, final Object... args) {
        String formatted;
        try {
            formatted = String.format(message, args);
        } catch (final RuntimeException ex) {
            try {
                formatted = message + Arrays.toString(args);
            } catch (final RuntimeException sol) {
                formatted = message + " [" + args.length + " args]";
            }
        }
        return formatted;
    }
}

/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package gm.util.log;

import android.util.Log;

import java.util.Map;

/**
 * Interface for use logger functionality.<br>
 * This is the interface that can be implemented as part of a decorator pattern.<br>
 * Each implementation forms a chain of {@link ILogger} that can manipulate the log string.<br>
 * Usually, the final logger will print the message to a file, or console.
 *
 * @hide
 */
public interface ILogger {

    /**
     * For 'custom' logger, this will indicate the local level to use.
     */
    String CUSTOM_LOG_LEVEL = "logLevel";

    /**
     * For 'custom' logger, this will indicate the tag to use optionally when logging.<br>
     * If 'null', the default tag is used.
     */
    String CUSTOM_LOG_TAG = "tag";

    /**
     * Log at Verbose level.
     *
     * @param message Message with %s replacements where the args should go, as per String.format() specification.
     *                This avoids unnecessary string concatenation if no log is actually going to be made and makes the
     *                log line look a little cleaner.
     * @param args    Arguments for the message string. There should be one %s in the message string for each argument.
     *                If the last one is a Throwable then the stack trace is outputted and no %s in the format string
     *                for it should be included.
     */
    void v(String message, Object... args);

    /**
     * Log at Debug level.
     *
     * @param message Message with %s replacements where the args should go, as per String.format() specification.
     *                This avoids unnecessary string concatenation if no log is actually going to be made and makes the
     *                log line look a little cleaner.
     * @param args    Arguments for the message string. There should be one %s in the message string for each argument.
     *                If the last one is a Throwable then the stack trace is outputted and no %s in the format string
     *                for it should be included.
     */
    void d(String message, Object... args);

    /**
     * Log at Info level.
     *
     * @param message Message with %s replacements where the args should go, as per String.format() specification.
     *                This avoids unnecessary string concatenation if no log is actually going to be made and makes the
     *                log line look a little cleaner.
     * @param args    Arguments for the message string. There should be one %s in the message string for each argument.
     *                If the last one is a Throwable then the stack trace is outputted and no %s in the format string
     *                for it should be included.
     */
    void i(String message, Object... args);

    /**
     * Log at Warning level.
     *
     * @param message Message with %s replacements where the args should go, as per String.format() specification.
     *                This avoids unnecessary string concatenation if no log is actually going to be made and makes the
     *                log line look a little cleaner.
     * @param args    Arguments for the message string. There should be one %s in the message string for each argument.
     *                If the last one is a Throwable then the stack trace is outputted and no %s in the format string
     *                for it should be included.
     */
    void w(String message, Object... args);

    /**
     * Log at Error level.
     *
     * @param message Message with %s replacements where the args should go, as per String.format() specification.
     *                This avoids unnecessary string concatenation if no log is actually going to be made and makes the
     *                log line look a little cleaner.
     * @param args    Arguments for the message string. There should be one %s in the message string for each argument.
     *                If the last one is a Throwable then the stack trace is outputted and no %s in the format string
     *                for it should be included.
     */
    void e(String message, Object... args);

    /**
     * Log at WTF Level and Assert - Very critical failure.
     *
     * @param message Message with %s replacements where the args should go, as per String.format() specification.
     *                This avoids unnecessary string concatenation if no log is actually going to be made and makes the
     *                log line look a little cleaner.
     * @param args    Arguments for the message string. There should be one %s in the message string for each argument.
     *                If the last one is a Throwable then the stack trace is outputted and no %s in the format string
     *                for it should be included.
     */
    void wtf(String message, Object... args);

    /**
     * Log at a custom level with additional / custom arguments.
     *
     * @param properties Properties that will be used by decorated loggers. Decorated loggers can
     *                   ignore or use this. Refer to each logger for properties.
     * @param message    The message
     * @param args       The arguments
     */
    void custom(Map<String, Object> properties, String message, Object... args);

    /**
     * @return Implementation of {@link ILogger} that is being decorated by 'this' {@link ILogger},
     * or {@code null} if it is not decorating and is just sending the message on to its final destination.
     */
    ILogger getDecorated();

    /**
     * Enum to represent Log Levels available.
     */
    enum LogLevel {

        ALL(Integer.MIN_VALUE, "ALL"),
        VERBOSE(Log.VERBOSE, "VERBOSE"),
        DEBUG(Log.DEBUG, "DEBUG"),
        INFO(Log.INFO, "INFO"),
        WARN(Log.WARN, "WARN"),
        ERROR(Log.ERROR, "ERROR"),
        ASSERT(Log.ASSERT, "ASSERT"),
        OFF(Integer.MAX_VALUE, "OFF");

        /**
         * @see #getLevel()
         */
        private final int mLevel;

        /**
         * Description of the current Logger level.
         */
        private final String mDescription;

        /**
         * Constructor.
         *
         * @param level       The level (severity) of Logger.
         * @param description Description of the current Logger level.
         */
        LogLevel(final int level, final String description) {
            mLevel = level;
            mDescription = description;
        }

        /**
         * The level (severity) of Logger.
         *
         * @param logLevelStr  String value related to the level (severity) of Logger.
         * @param defaultValue Default value to use in case of no match found.
         * @return The level (severity) of Logger.
         */
        static public LogLevel getLogLevel(final String logLevelStr, final LogLevel defaultValue) {
            LogLevel level = defaultValue;
            if (logLevelStr == null || logLevelStr.equalsIgnoreCase(OFF.mDescription)) {
                level = OFF;
            } else if (logLevelStr.equalsIgnoreCase(ALL.mDescription)) {
                level = ALL;
            } else if (logLevelStr.equalsIgnoreCase(VERBOSE.mDescription)) {
                level = VERBOSE;
            } else if (logLevelStr.equalsIgnoreCase(DEBUG.mDescription)) {
                level = DEBUG;
            } else if (logLevelStr.equalsIgnoreCase(INFO.mDescription)) {
                level = INFO;
            } else if (logLevelStr.equalsIgnoreCase(WARN.mDescription)) {
                level = WARN;
            } else if (logLevelStr.equalsIgnoreCase(ERROR.mDescription)) {
                level = ERROR;
            } else if (logLevelStr.equalsIgnoreCase(ASSERT.mDescription)) {
                level = ASSERT;
            }
            return level;
        }

        /**
         * @return The level (severity) of Logger.
         */
        public int getLevel() {
            return mLevel;
        }
    }
}

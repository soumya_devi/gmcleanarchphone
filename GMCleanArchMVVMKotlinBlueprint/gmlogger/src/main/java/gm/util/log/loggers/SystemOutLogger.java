/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package gm.util.log.loggers;

import java.util.Map;

import gm.util.log.ILogger;

/**
 * Implementation of the {@link ILogger} designed to use System's {@link System#out} to print messages.<br>
 *
 * @hide
 */
public class SystemOutLogger extends AbstractILogger {

    /**
     * Constructor that accepts {@link ILogger} interface.
     *
     * @param logger Optional {@link ILogger} interface that will be called post-output.
     */
    public SystemOutLogger(final ILogger logger) {
        super(logger);
    }

    /**
     * Default constructor.
     */
    public SystemOutLogger() {
        this(null);
    }

    @Override
    protected void logv(final String message, final Object... args) {
        System.out.println("V/" + message);
    }

    @Override
    protected void logd(final String message, final Object... args) {
        System.out.println("D/" + message);
    }

    @Override
    protected void logi(final String message, final Object... args) {
        System.out.println("I/" + message);
    }

    @Override
    protected void logw(final String message, final Object... args) {
        System.out.println("W/" + message);
    }

    @Override
    protected void loge(final String message, final Object... args) {
        System.err.println("E/" + message);
    }

    @Override
    protected void logwtf(final String message, final Object... args) {
        System.err.println("WTF/" + message);
        System.exit(-1);
    }

    @Override
    protected void logcustom(final Map<String, Object> properties, String message, final Object... args) {
        LogLevel level = null;
        String tag = null;
        if (properties != null) {
            level = (LogLevel) properties.get(CUSTOM_LOG_LEVEL);
            tag = (String) properties.get(CUSTOM_LOG_TAG);
        }
        if (level == null) {
            level = LogLevel.VERBOSE;
        }
        if (tag != null) {
            message = tag + "/" + message;
        }

        switch (level) {
            case VERBOSE:
                logv(message, args);
                break;
            case DEBUG:
                logd(message, args);
                break;
            case INFO:
                logi(message, args);
                break;
            case WARN:
                logw(message, args);
                break;
            case ERROR:
                loge(message, args);
                break;
            case ASSERT:
                logwtf(message, args);
                break;
            default:
                break;
        }
    }

}

/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.event

/**
 * Defines a component registering and unregistering to events. This enables the EventBus to unsubscribe a component
 * of all the events it subscribed to. ([EventBus.unregister])
 */
interface EventSubscriber

/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core

import com.gm.cleanarchbp.model.ModelComponent
import com.gm.core.annotation.ModelComponentBuilder

import java.lang.reflect.InvocationTargetException
import java.lang.reflect.Method

/**
 * Class allowing to build a model component from a method using @[ModelComponentBuilder]
 * through reflection. This allows to avoid a dependency from presentation to domain.
 */
object ModelInjector {

    /**
     * DO NOT MODIFY
     */
    private const val DOMAIN_INJECTOR_PATH = "com.gm.core.DomainInjector"

    /**
     * In charge of finding through reflection the method using @[ModelComponentBuilder]
     * and to call it to provide a ModelComponent instance
     *
     * @param application the application provided by presentation
     * @return the implementation instance of the [ModelComponent]
     */
    fun buildModelComponent(application: Any): ModelComponent {
        try {
            val domainInjector = Class.forName(DOMAIN_INJECTOR_PATH)
            val method = findBuilderMethod(domainInjector)
                    ?: throw IllegalStateException("Domain is not providing a way to build a model " + "component")
            return method.invoke(domainInjector.kotlin.objectInstance, application) as ModelComponent
        } catch (e: ClassNotFoundException) {
            throw RuntimeException(e)
        } catch (e: IllegalAccessException) {
            throw RuntimeException(e)
        } catch (e: InvocationTargetException) {
            throw RuntimeException(e)
        }

    }

    private fun findBuilderMethod(clazz: Class<*>?): Method? {
        if (clazz == null) {
            return null
        }
        val methods = clazz.methods
        for (method in methods) {
            if (isMatchingMethod(method)) {
                return method
            }
        }
        return null
    }

    private fun isMatchingMethod(method: Method): Boolean {
        val neededNumberOfArguments = 1
        return method.isAnnotationPresent(ModelComponentBuilder::class.java)
                && method.parameterCount == neededNumberOfArguments
    }
}

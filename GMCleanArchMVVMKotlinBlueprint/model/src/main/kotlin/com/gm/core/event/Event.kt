/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.event

/**
 * Parent interface for every event emitted through the [EventBus].
 */
interface Event {
    companion object {
        val NO_OP: Event = object : Event {
            //no-op
        }
    }
}

/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.executor

import io.reactivex.Scheduler

/**
 * Interface to be implemented to define the default schedulers to be used for the main thread and for the
 * background thread
 */
interface ScheduledExecutor {
    val mainThreadScheduler: Scheduler

    val backgroundScheduler: Scheduler
}

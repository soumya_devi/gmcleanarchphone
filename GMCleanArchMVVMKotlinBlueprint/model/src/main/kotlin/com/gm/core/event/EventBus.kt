/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */
package com.gm.core.event

import io.reactivex.functions.Consumer

/**
 * Allows publish-subscribe-style communication between components without requiring the components to explicitly
 * register with one another (and thus be aware of each other). It is designed exclusively to replace
 * traditional Java in-process event distribution using explicit registration. It is not a general-purpose
 * publish-subscribe system, nor is it intended for interprocess communication.
 */
interface EventBus {

    /**
     * Publishes an event through the bus. Every EventSubscriber for that type of event will receive it.
     *
     * @param event the event to be sent
     * @since 0.1.0
     */
    fun post(event: Event)

    /**
     * Enables an EventSubscriber to receive some event of type T. A consumer needs to be provided to
     * define the reaction to this event
     *
     * @param eventType       the class of the event subscribed to
     * @param eventSubscriber the entity subscribing to the event
     * @param consumer        the provided consumer
     * @param <T>             the type of event
     * @since 0.1.0
     */
    fun <T : Event> register(eventType: Class<T>,
                             eventSubscriber: EventSubscriber,
                             consumer: Consumer<in T>)

    /**
     * Enables an EventSubscriber to receive some event of type T. A consumer needs to be provided to
     * define the reaction to this event
     *
     * @param eventType       the class of the event subscribed to
     * @param eventSubscriber the entity subscribing to the event
     * @param consumer        the provided consumer lambda
     * @param <T>             the type of event
     * @since 0.1.0
     */
    fun <T : Event> register(eventType: Class<T>,
                             eventSubscriber: EventSubscriber,
                             consumer: (T) -> Unit)

    /**
     * Disables any registration for a given event subscriber
     *
     * @param eventSubscriber the subscriber to be unsubscribed
     * @since 0.1.0
     */
    fun unregister(eventSubscriber: EventSubscriber)

    /**
     * Disable the registration for a given event type and a subscriber
     *
     * @param eventType       the class of the event subscribed to
     * @param eventSubscriber the entity subscribing to the event
     * @param <T>             the type of event
     * @since 0.1.0
    </T> */
    fun <T : Event> unregister(eventType: Class<T>, eventSubscriber: EventSubscriber)
}

/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */
package com.gm.core.usecase

import com.gm.core.executor.ScheduledExecutor
import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableCompletableObserver
import java.util.logging.Level
import java.util.logging.Logger

/**
 * Defines application business logic using [io.reactivex.Completable] as the main
 * reactive component. Please refer to [io.reactivex.Completable] documentation to
 * understand the ins and outs of this class.
 *
 *
 * This is mostly useful to trigger operations which do not retrieve any data but tell domain components to do
 * some operation. (e.g. placing a call, resync contacts, etc...)
 *
 *
 * This simplifies the observer implementation by only having to implement
 * [CompletableObserver.onComplete] and
 * [CompletableObserver.onError]
 *
 *
 *
 * @see ObservableUseCase
 *
 * @see SingleUseCase
 */
abstract class CompletableUseCase protected constructor(protected val mScheduledExecutor: ScheduledExecutor) {
    private val mDisposables: CompositeDisposable = CompositeDisposable()

    /**
     * Method called when [CompletableUseCase.execute] is called. It prepares the
     * [io.reactivex.Completable] and interacts with domain components.
     *
     * @return a [io.reactivex.Completable]
     */
    protected abstract fun setupCompletable(): Completable

    /**
     * Executes the use case by:
     *
     * - Preparing the chain of operations with [CompletableUseCase.setupCompletable]
     * - Setting up the use case to be run in a background thread and to receive the result on the main thread.
     * - Binding the observer to it to propagate the result when it is available.
     *
     * @param observer the callback to retrieve the result of this use case.
     * @param <T>      a type implementing [Disposable] and [CompletableObserver]
     * (e.g. [io.reactivex.observers.DisposableCompletableObserver]
     * @since 0.1.0
     */
    fun <T> execute(observer: T) where T : Disposable, T : CompletableObserver {
        val completable = this.setupCompletable()
                .subscribeOn(mScheduledExecutor.backgroundScheduler)
                .observeOn(mScheduledExecutor.mainThreadScheduler)
                .doOnSubscribe {
                    Logger.getLogger(javaClass.simpleName)
                            .log(Level.INFO, "executed")
                }
        addDisposable(completable.subscribeWith(observer))
    }

    /**
     * Executes the use case by:
     *
     * - Preparing the chain of operations with [CompletableUseCase.setupCompletable]
     * - Setting up the use case to be run in a background thread and to receive the result on the main thread.
     * - Binding the observer to it to propagate the result when it is available.
     * - The observer will call each respective input method
     *
     * @param onComplete the value function telling the observer that the stream has closed.
     * ([DisposableCompletableObserver.onComplete]).
     * @param onError the value-function to provide a [Throwable] in case of an error
     * ([DisposableCompletableObserver.onError]).
     */
    inline fun execute(crossinline onComplete: () -> Unit,
                       crossinline onError: (Throwable) -> Unit) {
        execute(object : DisposableCompletableObserver() {
            override fun onComplete() = onComplete()

            override fun onError(throwable: Throwable) = onError(throwable)
        })
    }

    /**
     * Provides a way to chain the resulting [Completable] to some other operations
     *
     * DOES NOT RUN ON ANY PARTICULAR SCHEDULER.
     *
     * @return a [Completable]
     * @since 0.1.0
     */
    fun chain(): Completable {
        return setupCompletable()
    }

    /**
     * Clear the use case to cancel its operations and to prevent the observer from being called.
     *
     * @see CompositeDisposable.clear
     * @since 0.1.0
     */
    fun clear() {
        mDisposables.clear()
    }

    protected fun addDisposable(disposable: Disposable) {
        mDisposables.add(disposable)
    }
}

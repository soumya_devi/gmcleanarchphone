/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.annotation

/**
 * Annotation used by any implementation of the Model Component to attach its implementation at runtime
 *
 *
 * Example:
 * <pre>
 * public class DomainInjector {
 * private static DomainComponent sDomainComponent;
 *
 * \@ModelComponentBuilder
 * public static ModelComponent buildDomainComponent(final Object application){
 * sDomainComponent=DaggerDomainComponent.builder().application(application).build();
 * return sDomainComponent;
 * }
 *
 * public static DomainComponent getDomainComponent(){
 * if(sDomainComponent==null){
 * throw new IllegalStateException("Attempt to access the domain component before it has been built");
 * }
 * return sDomainComponent;
 * }
 * }
</pre> *
 */
@Target(AnnotationTarget.FUNCTION, AnnotationTarget.PROPERTY_GETTER, AnnotationTarget.PROPERTY_SETTER)
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class ModelComponentBuilder

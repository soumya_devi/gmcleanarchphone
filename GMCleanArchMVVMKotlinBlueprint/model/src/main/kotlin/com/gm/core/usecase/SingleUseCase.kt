/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.usecase

import com.gm.core.executor.ScheduledExecutor
import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import java.util.logging.Level
import java.util.logging.Logger

/**
 * Defines application business logic using [io.reactivex.Single] as the main reactive component.
 * Please refer to [io.reactivex.Single] documentation to understand the ins and outs of this class.
 *
 *
 * This is mostly useful to trigger operations retrieving a single chunk of data at once.
 * (e.g. retrieve all favorites, get the outgoing device, etc...)
 *
 *
 * This enables to simplify the observer implementation by only having to implement
 * [io.reactivex.SingleObserver.onSuccess] and
 * [SingleObserver.onError]
 *
 * @param <Output> The type of object that can be observed
 * @see ObservableUseCase
 *
 * @see CompletableUseCase
</Output> */
abstract class SingleUseCase<Output> protected constructor(protected val mScheduledExecutor: ScheduledExecutor) {
    private val mDisposables: CompositeDisposable = CompositeDisposable()

    /**
     * Method called when [SingleUseCase.execute] is called. It prepares the
     * [io.reactivex.Single] and interacts with domain components.
     *
     * @return a [io.reactivex.Single]
     */
    protected abstract fun setupSingle(): Single<Output>

    /**
     * Executes the use case by:
     *
     *  - Preparing the chain of operations with [SingleUseCase.setupSingle]
     *  - Setting up the use case to be run in a background thread and to receive the result on the main thread.
     *  - Binding the observer to it to propagate the result when it is available.
     *
     * @param observer the callback to retrieve the result of this use case.
     * @param <T>      a type implementing [Disposable] and [SingleObserver]
     * (e.g. [io.reactivex.observers.DisposableSingleObserver]
     * @since 0.1.0
     */
    fun <T> execute(observer: T) where T : Disposable, T : SingleObserver<Output> {
        val single = this.setupSingle()
                .subscribeOn(mScheduledExecutor.backgroundScheduler)
                .observeOn(mScheduledExecutor.mainThreadScheduler)
                .doOnSubscribe {
                    Logger.getLogger(javaClass.simpleName)
                            .log(Level.INFO, "executed")
                }
        addDisposable(single.subscribeWith(observer))
    }

    /**
     * Executes the use case by:
     *
     *  - Preparing the chain of operations with [SingleUseCase.setupSingle]
     *  - Setting up the use case to be run in a background thread and to receive the result on the main thread.
     *  - Binding the observer to it to propagate the result when it is available.
     *  - The observer will call each respective input method
     *
     * @param onSuccess the value-function to provide the [Output] when the stream provides the single output
     * ([SingleObserver.onSuccess]).
     * @param onError the value-function to provide a [Throwable] in case of an error ([SingleObserver.onError]).
     * @since 0.1.0
     */
    inline fun execute(crossinline onSuccess: (Output) -> Unit,
                       crossinline onError: (Throwable) -> Unit) {
        execute(object : DisposableSingleObserver<Output>() {
            override fun onError(throwable: Throwable) = onError(throwable)

            override fun onSuccess(output: Output) = onSuccess(output)
        })
    }

    /**
     * Provides a way to chain the resulting [Single] to some other operations
     *
     * DOES NOT RUN ON ANY PARTICULAR SCHEDULER.
     *
     * @return a [Single]
     * @since 0.1.0
     */
    fun chain(): Single<Output> {
        return setupSingle()
    }

    /**
     * Clear the use case to cancel its operations and to prevent the observer from being called.
     *
     * @see CompositeDisposable.clear
     * @since 0.1.0
     */
    fun clear() {
        mDisposables.clear()
    }

    protected fun addDisposable(disposable: Disposable) {
        mDisposables.add(disposable)
    }

}

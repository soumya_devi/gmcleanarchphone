/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.usecase


import com.gm.core.executor.ScheduledExecutor

import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableObserver
import java.util.logging.Level
import java.util.logging.Logger

/**
 * Defines application business logic using [io.reactivex.Observable] as the main reactive component.
 * Please refer to [io.reactivex.Observable] documentation to understand the ins and outs of this class.
 *
 *
 * This is mostly useful to trigger operations requiring streaming with a large amount of data.
 *
 *
 * The Observer for this type of use case contains 3 methods:
 * [Observer.onNext],
 * [Observer.onError] and
 * [Observer.onComplete].
 *
 *
 *
 * @param <Output> The type of object that can be observed
 * @see CompletableUseCase
 *
 * @see SingleUseCase
</Output> */
abstract class ObservableUseCase<Output> protected constructor(protected val mScheduledExecutor: ScheduledExecutor) {
    private val mDisposables: CompositeDisposable = CompositeDisposable()

    /**
     * Method called when [ObservableUseCase.execute] is called. It prepares the
     * [io.reactivex.Observable] and interacts with domain components.
     *
     * @return an [io.reactivex.Observable]
     */
    protected abstract fun setupObservable(): Observable<Output>

    /**
     * Executes the use case by:
     *
     *  - Preparing the chain of operations with [ObservableUseCase.setupObservable]
     *  - Setting up the use case to be run in a background thread and to receive the result on the main thread.
     *  - Binding the observer to it to propagate the result when it is available.
     *
     * @param observer the callback to retrieve the result of this use case.
     * @param <T>      a type implementing [Disposable] and [Observer]
     * (e.g. [io.reactivex.observers.DisposableObserver]
     * @since 0.1.0
     */
    fun <T> execute(observer: T) where T : Disposable, T : Observer<Output> {
        val observable = this.setupObservable()
                .subscribeOn(mScheduledExecutor.backgroundScheduler)
                .observeOn(mScheduledExecutor.mainThreadScheduler)
                .doOnSubscribe {
                    Logger.getLogger(javaClass.simpleName).log(Level.INFO, "executed")
                }
        addDisposable(observable.subscribeWith(observer))
    }

    /**
     * Executes the use case by:
     *
     *  - Preparing the chain of operations with [ObservableUseCase.setupObservable]
     *  - Setting up the use case to be run in a background thread and to receive the result on the main thread.
     *  - Binding the observer to it to propagate the result when it is available.
     *  - The observer will call each respective input method
     *
     * @param onNext the value-function to provide the [Output] ([DisposableObserver.onNext]).
     * @param onError the value-function to provide a [Throwable] in case of an error ([DisposableObserver.onError]).
     * @param onComplete the value function telling the observer that the stream has closed
     * ([DisposableObserver.onComplete]).
     * @since 0.1.0
     */
    inline fun execute(crossinline onNext: (Output) -> Unit,
                       crossinline onError: (Throwable) -> Unit,
                       crossinline onComplete: () -> Unit = {}) {
        execute(object : DisposableObserver<Output>() {
            override fun onComplete() = onComplete()

            override fun onNext(output: Output) = onNext(output)

            override fun onError(error: Throwable) = onError(error)
        })
    }

    /**
     * Provides a way to chain the resulting [Observable] to some other operations
     *
     * DOES NOT RUN ON ANY PARTICULAR SCHEDULER.
     *
     * @return a [Observable]
     * @since 0.1.0
     */
    fun chain(): Observable<Output> {
        return setupObservable()
    }

    /**
     * Clear the use case to cancel its operations and to prevent the observer from being called.
     *
     * @see CompositeDisposable.clear
     * @since 0.1.0
     */
    fun clear() {
        mDisposables.clear()
    }

    protected fun addDisposable(disposable: Disposable) {
        mDisposables.add(disposable)
    }
}

package com.gm.cleanarchbp.model.usecase

import com.gm.cleanarchbp.model.gateway.ChronoGateway
import com.gm.core.executor.ScheduledExecutor
import com.gm.core.usecase.CompletableUseCase
import io.reactivex.Completable
import javax.inject.Inject

class ResetChronoUseCase @Inject
internal constructor(
        scheduledExecutor: ScheduledExecutor,
        private val chronoGateway: ChronoGateway
) : CompletableUseCase(scheduledExecutor) {
    override fun setupCompletable(): Completable {
        return chronoGateway.resetChrono()
    }
}

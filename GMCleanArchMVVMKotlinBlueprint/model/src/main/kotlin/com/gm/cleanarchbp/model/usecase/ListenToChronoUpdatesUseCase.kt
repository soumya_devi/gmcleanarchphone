/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.model.usecase

import com.gm.cleanarchbp.model.gateway.ChronoGateway
import com.gm.core.executor.ScheduledExecutor
import com.gm.core.usecase.ObservableUseCase
import io.reactivex.Observable
import javax.inject.Inject

class ListenToChronoUpdatesUseCase @Inject
internal constructor(
        scheduledExecutor: ScheduledExecutor,
        private val chronoGateway: ChronoGateway
) : ObservableUseCase<Void>(scheduledExecutor) {
    override fun setupObservable(): Observable<Void> {
        return Observable.create { emitter ->
            val disposable = chronoGateway.listenToChronoFeatureChange()
            emitter.setDisposable(disposable)
        }
    }
}

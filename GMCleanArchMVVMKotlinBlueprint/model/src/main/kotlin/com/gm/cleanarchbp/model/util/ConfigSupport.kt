package com.gm.cleanarchbp.model.util

import com.gm.cleanarchbp.model.entity.BrandType

interface ConfigSupport {

    fun getCurrentBrand(): BrandType
}
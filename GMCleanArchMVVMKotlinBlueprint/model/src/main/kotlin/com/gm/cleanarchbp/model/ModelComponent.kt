/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.model


import com.gm.cleanarchbp.model.gateway.ChronoGateway
import com.gm.cleanarchbp.model.gateway.H2G2Gateway
import com.gm.cleanarchbp.model.util.ConfigSupport
import com.gm.core.ModelInjector
import com.gm.core.event.EventBus
import com.gm.core.executor.ScheduledExecutor

import dagger.Component

/**
 * Any component bound to the model shall extend this component.
 * Use the [ModelInjector] to bind it to the DaggerModelComponent instance with the depending
 * module.
 */
@Component
interface ModelComponent {
    fun scheduledExecutor(): ScheduledExecutor

    fun eventBus(): EventBus

    fun h2g2Gateway(): H2G2Gateway

    fun chronoGateway(): ChronoGateway

    fun configSupport(): ConfigSupport
}

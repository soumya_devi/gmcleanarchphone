/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.model.usecase

import com.gm.cleanarchbp.model.entity.PBContactsListDetails
import com.gm.cleanarchbp.model.gateway.H2G2Gateway
import com.gm.cleanarchbp.model.usecase.model.AnswerFound
import com.gm.cleanarchbp.model.usecase.model.AnswerNotFound
import com.gm.cleanarchbp.model.usecase.model.FindSomeFeatureResult
import com.gm.core.executor.ScheduledExecutor
import com.gm.core.usecase.SingleUseCase
import io.reactivex.Single
import javax.inject.Inject

class FindTheAnswerToTheUniverseUseCase @Inject
internal constructor(
        scheduledExecutor: ScheduledExecutor,
        private val h2G2Gateway: H2G2Gateway
) : SingleUseCase<FindSomeFeatureResult>(scheduledExecutor) {
    override fun setupSingle(): Single<FindSomeFeatureResult> {
        return h2G2Gateway.findAnAnswerToTheUniverse()
                .filter { it.value == THE_ANSWER_TO_THE_UNIVERSE_AND_EVERYTHING_ELSE }
                .map<FindSomeFeatureResult> { AnswerFound(it) }
                .toSingle(AnswerNotFound)
    }

    fun contactList(): ArrayList<PBContactsListDetails> {
        return h2G2Gateway.getAllContactsByPage()
    }

    companion object {
        private const val THE_ANSWER_TO_THE_UNIVERSE_AND_EVERYTHING_ELSE = 42L
    }
}

/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */
package com.gm.cleanarchbp.model.entity

data class DeepThoughtAnswer(val name: String, val value: Long, val char: Char)

data class PBContactsListDetails(var contactHandle: Int, var firstName: String, var lastName: String, var phoneNumber: String , var contactHeaderDetailsMap: LinkedHashMap<String, Int>?)





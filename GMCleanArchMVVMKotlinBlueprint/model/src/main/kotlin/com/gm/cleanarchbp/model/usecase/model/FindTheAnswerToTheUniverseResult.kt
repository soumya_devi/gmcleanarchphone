/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.model.usecase.model

import com.gm.cleanarchbp.model.entity.DeepThoughtAnswer

sealed class FindSomeFeatureResult
data class AnswerFound(val deepThoughtAnswer: DeepThoughtAnswer) : FindSomeFeatureResult()
object AnswerNotFound : FindSomeFeatureResult()
/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.model.entity.event

import com.gm.cleanarchbp.model.entity.Chrono
import com.gm.core.event.Event

/**
 * Example of a feature event
 */
data class ChronoEvent(val chrono: Chrono) : Event

package com.gm.cleanarchbp.model.gateway

import io.reactivex.Completable
import io.reactivex.disposables.Disposable

interface ChronoGateway {

    /**
     * Listen to some kind of chrono features which emits from time to time and remains active all along the
     * application lifetime
     *
     * @return a disposable
     */
    fun listenToChronoFeatureChange(): Disposable

    fun resetChrono(): Completable
}

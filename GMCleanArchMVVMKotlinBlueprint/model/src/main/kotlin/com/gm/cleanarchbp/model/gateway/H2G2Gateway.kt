/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */
package com.gm.cleanarchbp.model.gateway

import com.gm.cleanarchbp.model.entity.DeepThoughtAnswer
import com.gm.cleanarchbp.model.entity.PBContactsListDetails

import io.reactivex.Single

interface H2G2Gateway {

    /**
     * Example of API provided by this H2G2 Gateway
     *
     * @return a [Single] Feature
     */
    fun findAnAnswerToTheUniverse(): Single<DeepThoughtAnswer>
    fun getAllContactsByPage():ArrayList<PBContactsListDetails>
}

<!-- You can open the README.html file in your browser for a better rendering of this documentation.-->
# GM Clean Architecture Blueprint (Java with MVP)

This project defines a skeleton to be used to setup your initial project in with Clean Architecture. More specifically, this version provides the whole project in Java with MVP. The libraries used from the beginning are RxJava and Dagger.

This project gives an example with _UseCases_ and _EventBus_ to load features. _FindSomeFeatureUseCase_ loads a feature after 2 seconds when it's called. _ListenToFeatureUpdatesUseCase_ is listening to some feature which provides events regularly all along the application lifetime. More specifically this send a chrono event every seconds. Leaving the app and coming back to the app does not restart the chrono. The purpose being to demonstrate the power of dagger with scope management.

This project has been built base on [GMPhone MY22](https://share.gm.com/sites/crecinfotainmentsw/Shared%20Documents/Software%20Documents/Core%20Apps/GMPhone/Wiki/index.html "GMPhone Wiki") with a few changes:

- GMPhone had proxy interfaces in the model. This did not really respect the Proxy design pattern as only a second implementation of the interface using the first one should be called Proxy. Therefore, those interfaces have been changed to Gateway.
- GMPhone was using dagger 2.19 which was using AndroidInjector.Builder. This blueprint uses AndroidInjector.Factory instead (introduced in dagger 2.22).
- GMPhone uses only the scheduled executor directly in the UseCases instead of having a background executor AND a scheduled executor which was redundant.

More examples of _UseCases_ and _EventBus_ usages can be found in GMPhone if needed.

package com.gm.cleanarchbp.domain

import android.util.Log
import com.gm.core.usecase.CompletableUseCase
import com.gm.core.usecase.ObservableUseCase
import com.gm.core.usecase.SingleUseCase
import io.reactivex.observers.DisposableCompletableObserver
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.DisposableSingleObserver

inline fun <V : Any /*ReturnType*/> SingleUseCase<V>.onSuccess(
        crossinline onSuccess: (V?) -> Unit
) {
    execute(object : DisposableSingleObserver<V>() {

        override fun onSuccess(v: V) {
            onSuccess(v)
            clear()
        }

        override fun onError(throwable: Throwable) {
            Log.d("SDKManger", "${throwable.message}: ${throwable.printStackTrace()}")
            clear()
        }
    })
}

inline fun <V : Any /*ReturnType*/> ObservableUseCase<V>.onNext(
        crossinline onNext: (V?) -> Unit
) {
    execute(object : DisposableObserver<V>() {
        override fun onComplete() {
            val className = this::class.java.simpleName
            Log.d("SDKManger", "$className: onComplete")
            clear()
        }

        override fun onNext(v: V) {
            onNext(v)
        }

        override fun onError(throwable: Throwable) {
            Log.d("SDKManger", "${throwable.message}: ${throwable.printStackTrace()}")
            clear()
        }

    })
}

inline fun CompletableUseCase.onComplete(
        crossinline onComplete: () -> Unit
) {
    execute(object : DisposableCompletableObserver() {
        override fun onComplete() {
            onComplete()
            clear()
        }

        override fun onError(throwable: Throwable) {
            Log.d("SDKManger", "${throwable.message}: ${throwable.printStackTrace()}")
            clear()
        }

    })
}
/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */
package com.gm.cleanarchbp.domain

import android.app.Application
import android.content.Context
import com.gm.core.annotation.ApplicationContext
import com.gm.core.executor.AndroidScheduledExecutor
import com.gm.core.executor.ScheduledExecutor
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DomainModule {

    /**
     * DO NOT MODIFY
     * This binds the application object to a Context (@[ApplicationContext])
     *
     * @param application the application instance provided by presentation
     * @return the context
     */
    @Provides
    @Singleton
    @ApplicationContext
    fun applicationContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    fun scheduledExecutor(androidScheduledExecutor: AndroidScheduledExecutor): ScheduledExecutor {
        return androidScheduledExecutor
    }
}

package com.gm.cleanarchbp.domain.util

import com.gm.cleanarchbp.model.entity.BrandType
import com.gm.cleanarchbp.model.util.ConfigSupport
import javax.inject.Inject
import kotlin.math.abs
import kotlin.random.Random


class ConfigSupportImpl @Inject constructor() : ConfigSupport {
    override fun getCurrentBrand(): BrandType =
            when (1) {
               // 0 -> BrandType.CHEVY
                1 -> BrandType.CADILLAC
              //  2 -> BrandType.CADILLAC_FREE_FORM
              //  3 -> BrandType.GMC
                else -> BrandType.CADILLAC
            }
}
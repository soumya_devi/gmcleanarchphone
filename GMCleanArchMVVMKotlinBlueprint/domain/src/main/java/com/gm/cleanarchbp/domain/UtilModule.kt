package com.gm.cleanarchbp.domain

import com.gm.cleanarchbp.domain.util.ConfigSupportImpl
import com.gm.cleanarchbp.model.util.ConfigSupport
import dagger.Binds
import dagger.Module
import dagger.Reusable

@Module
abstract class UtilModule {

    @Binds
    @Reusable
    internal abstract fun configSupport(configSupport: ConfigSupportImpl): ConfigSupport
}
/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.domain

import com.gm.cleanarchbp.domain.chrono.ChronoGatewayImpl
import com.gm.cleanarchbp.domain.h2g2.H2G2GatewayImpl
import com.gm.cleanarchbp.model.gateway.ChronoGateway
import com.gm.cleanarchbp.model.gateway.H2G2Gateway
import dagger.Binds
import dagger.Module
import dagger.Reusable
import javax.inject.Singleton

@Module
abstract class GatewayModule {

    @Binds
    @Reusable
    internal abstract fun featureGateway(featureGateway: H2G2GatewayImpl): H2G2Gateway

    @Binds
    @Singleton
    internal abstract fun chronoGateway(chronoGateway: ChronoGatewayImpl): ChronoGateway
}

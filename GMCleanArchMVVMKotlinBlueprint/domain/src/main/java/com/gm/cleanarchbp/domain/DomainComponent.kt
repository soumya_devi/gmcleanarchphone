/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */
package com.gm.cleanarchbp.domain

import com.gm.cleanarchbp.model.ModelComponent
import com.gm.core.event.EventModule
import dagger.BindsInstance
import dagger.Component
import javax.inject.Singleton

/**
 * Specialization of Model Component. Provides to the outside component the classes defined in
 * ModelComponent as well as the classes defined in this component (e.g. Context)
 * It includes every modules from domain, as well as the ModelModule
 */
@Singleton
@Component(modules = [
    DomainModule::class,
    EventModule::class,
    GatewayModule::class,
    UtilModule::class
])
interface DomainComponent : ModelComponent {

    @Component.Factory
    interface Factory {

        /**
         * Binds the application instance to this component.
         * This is used to avoid the use of a constructor in the DomainModule.
         * If this is not added, the component would have to be built like this:
         *
         * `DaggerDomainComponent.Builder().domainModule(new DomainModule(application).build()`.
         *
         * Thanks to [BindsInstance], no constructor is added in the module and the component is
         * created as follow:
         *
         * `DaggerDomainComponent.factory().create(application)`
         *
         * Previous versions of dagger were using a Builder which initialized as follow:
         * `DaggerDomainComponent.Builder().application(application).build()`
         *
         * @param application the application instance provided by presentation
         * @return the Builder instance
         */
        fun create(@BindsInstance application: Any): DomainComponent
    }
}

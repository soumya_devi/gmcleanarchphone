package com.gm.cleanarchbp.domain.chrono

import com.gm.cleanarchbp.model.entity.Chrono
import com.gm.cleanarchbp.model.entity.event.ChronoEvent
import com.gm.cleanarchbp.model.gateway.ChronoGateway
import com.gm.core.event.EventBus
import io.reactivex.Completable
import io.reactivex.Observable
import io.reactivex.disposables.Disposable
import io.reactivex.subjects.PublishSubject
import io.reactivex.subjects.Subject
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class ChronoGatewayImpl @Inject constructor(private val mEventBus: EventBus) : ChronoGateway {

    private val resetSubject: Subject<Unit>
    private val mChronoObservable: Observable<ChronoEvent>

    init {
        resetSubject = PublishSubject.create<Unit>()
        mChronoObservable = resetSubject
                .startWith(Unit)
                .switchMap {
                    Observable.interval(1, TimeUnit.SECONDS)
                            .map { t -> ChronoEvent(Chrono("You have been waiting for", t)) }
                            .replay(1)
                            .autoConnect()
                }
    }

    override fun listenToChronoFeatureChange(): Disposable {
        return mChronoObservable.subscribe { mEventBus.post(it) }
    }

    override fun resetChrono(): Completable {
        return Completable.fromAction { resetSubject.onNext(Unit) }
    }
}
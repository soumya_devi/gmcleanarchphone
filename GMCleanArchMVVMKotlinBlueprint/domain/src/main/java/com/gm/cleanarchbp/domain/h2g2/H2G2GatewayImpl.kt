/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.cleanarchbp.domain.h2g2

import android.util.Log
import com.gm.cleanarchbp.domain.onNext
import com.gm.cleanarchbp.model.entity.DeepThoughtAnswer
import com.gm.cleanarchbp.model.entity.PBContactsListDetails
import com.gm.cleanarchbp.model.gateway.H2G2Gateway
import com.gm.phone.model.usecase.GetAllContactsByPageUseCase
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class H2G2GatewayImpl @Inject constructor() : H2G2Gateway {

    override fun findAnAnswerToTheUniverse(): Single<DeepThoughtAnswer> {
        return Single.just(DeepThoughtAnswer(
                "The answer to the universe and everything else",
                42,
                'z')
        ).delay(2, TimeUnit.SECONDS)
    }

    override fun getAllContactsByPage(): ArrayList<PBContactsListDetails> {
        val contactList = ArrayList<PBContactsListDetails>()
        contactList.clear()
        contactList.add(PBContactsListDetails(1, "siddu"
                ?: "", "vsp", "9494927054"
                ?: "", LinkedHashMap()))
        return contactList
    }

    /*fun getContact(){
        val getAllContactsByPageUseCase : GetAllContactsByPageUseCase=
        val contactList = java.util.ArrayList<PBContactsListDetails>()
        val contactHeaderMap = java.util.LinkedHashMap<String, Int>()
        getAllContactsByPageUseCase.get().clear()
        getAllContactsByPageUseCase.get().onNext {
            contactList.clear()
            contactHeaderMap.clear()
            it?.map {
                contactHeaderMap[it.pageId] = it.contacts.size
                //GMLog.d(mTag, "it.pageId " + it.pageId + " it.contacts.size " + it.contacts.size)
                val phone = it?.contacts?.map {
                    PBContactsListDetails(it.id.toInt(), it.fullName
                            ?: "", it.fullName ?: "", it.fullName
                            ?: "", java.util.LinkedHashMap())
                }
                phone!![0].contactHeaderDetailsMap = contactHeaderMap
                contactList.addAll(phone)
            }
           // contactsResHandler.get().onGetAllContactsByPage(PBContactsList(ZERO, ZERO, contactList.size, contactList, contactHeaderMap))
        }
    }*/
}

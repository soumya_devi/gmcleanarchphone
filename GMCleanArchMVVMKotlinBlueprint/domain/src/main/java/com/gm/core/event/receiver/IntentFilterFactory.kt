/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.event.receiver

import android.content.IntentFilter

interface IntentFilterFactory {

    fun createIntentFilter(vararg actionableIntentType: ActionableIntentType): IntentFilter
}

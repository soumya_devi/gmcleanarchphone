/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.event.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import java.util.concurrent.atomic.AtomicBoolean

/**
 * Rx version of a broadcast receiver
 * onReceive, calls the mObserver.onNext method
 * Unregister itself on dispose.
 */
class DisposableReceiver(private val mContext: Context, private val mObserver: Observer<in Intent>) : BroadcastReceiver(), Disposable {
    private val mDisposed: AtomicBoolean

    init {
        requireNotNull(mContext) { "Provided Context cannot be null" }
        requireNotNull(mObserver) { "Provided Observer cannot be null" }
        mDisposed = AtomicBoolean(false)
    }

    override fun dispose() {
        if (mDisposed.compareAndSet(false, true)) {
            mContext.unregisterReceiver(this@DisposableReceiver)
        }
    }

    override fun isDisposed(): Boolean {
        return mDisposed.get()
    }

    override fun onReceive(context: Context, intent: Intent) {
        if (!isDisposed) {
            mObserver.onNext(intent)
        }
    }
}

/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.repository.specification

import android.net.Uri

/**
 * Specification defining the contract to trigger content provider queries
 */
interface ContentProviderSpecification : Specification {

    val uri: Uri

    val projection: Array<String>

    val selection: String

    val selectionArgs: Array<String>

    val sortOrder: String
}

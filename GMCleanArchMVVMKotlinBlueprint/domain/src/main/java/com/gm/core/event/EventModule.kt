/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.event

import com.gm.core.event.bus.RxBus
import com.gm.core.event.receiver.IntentFilterFactory
import com.gm.core.event.receiver.IntentFilterFactoryImpl
import dagger.Binds
import dagger.Module
import javax.inject.Singleton

@Module
abstract class EventModule {

    @Binds
    @Singleton
    internal abstract fun eventBus(rxBus: RxBus): EventBus

    @Binds
    internal abstract fun intentFilterFactory(intentFilterFactory: IntentFilterFactoryImpl): IntentFilterFactory
}

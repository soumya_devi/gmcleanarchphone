/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */
package com.gm.core.event.bus

import com.gm.core.event.Event
import io.reactivex.Observable
import io.reactivex.Scheduler
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Consumer
import io.reactivex.subjects.BehaviorSubject

/**
 * A bus publisher adds features to a subject to be able to control more thoroughly the filtering and
 * the caching for this publisher.
 *
 * @param <T> the type of object this bus publisher can handle (i.e. Event)
</T> */
internal class BusPublisher private constructor(private val mObserveOnScheduler: Scheduler) {

    private val mSubject: BehaviorSubject<Event> = BehaviorSubject.create()

    /**
     * Posts an event on the subject. This event is posted independently on whether or not there are subscribers for
     * this event. The posted value will be cached due to the properties of [BehaviorSubject].
     *
     * @param event the published event
     */
    fun publish(event: Event) {
        mSubject.onNext(event)
    }

    /**
     * This method will set up the filter on any given eventType propagated in the subject, and subscribe it to the
     * given consumer.
     * The used subject is a behavior subject. Therefore, if an event is sent to published
     * [BusPublisher.publish] on this subject, this event is stored within the behavior
     * subject as a cached value. If there is a cached value at the moment of the subscription, the consumer will
     * receive this cached value right away.
     * Warning is suppressed due to implicit generics creating unchecked assignment warning.
     *
     * @param eventType the type of events the consumer wants to receive
     * @param consumer  the consumer receiving that event
     * @return the subscribed observable as disposable.
     */
    fun subscribeWithCache(eventType: Class<out Event>, consumer: Consumer<in Event>): Disposable {
        return mSubject.ofType(eventType).observeOn(mObserveOnScheduler).subscribe(consumer)
    }

    /**
     * This method will set up the filter on any given eventType propagated in the subject, and subscribe it to the
     * given consumer. This method also make sure no cache value is emitted upon subscription.
     * The used subject is a behavior subject. Therefore, if an event is sent to published
     * [BusPublisher.publish] on this subject, this event is stored within the behavior
     * subject as a cached value. Because this method shall discard this cached value, the [Observable.skip]
     * operator will take care of discarding it. However, that first value should only be discarded if it is the cached
     * value. Using [io.reactivex.subjects.BehaviorSubject.hasValue] enables to make that difference.
     * Warning is suppressed due to implicit generics creating unchecked assignment warning
     *
     * @param eventType the type of event the consumer wants to receive
     * @param consumer  the consumer receiving that event
     * @return the subscribed observable as disposable.
     */
    fun <T> subscribeWithoutCache(eventType: Class<out T>, consumer: Consumer<in T>): Disposable {
        val filteredEventObservable = mSubject.ofType(eventType).observeOn(mObserveOnScheduler)
        return if (mSubject.hasValue()) {
            filteredEventObservable.skip(EXISTING_VALUE.toLong()).subscribe(consumer)
        } else {
            filteredEventObservable.subscribe(consumer)
        }
    }

    companion object {

        private const val EXISTING_VALUE = 1

        fun create(observeOnScheduler: Scheduler): BusPublisher {
            return BusPublisher(observeOnScheduler)
        }
    }
}
/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.repository

import android.content.ContentProviderOperation
import android.content.ContentValues
import android.net.Uri

import javax.inject.Inject

class ContentProviderOperationFactory @Inject constructor() {
    fun createInsertOperation(uri: Uri, contentValues: ContentValues): ContentProviderOperation {
        return ContentProviderOperation.newInsert(uri).withValues(contentValues).build()
    }
}

/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.repository


import com.gm.core.repository.specification.Specification
import io.reactivex.functions.Function

interface ReadableRepository<T, S : Specification, DBO> {

    fun findAll(specification: S, mapper: Function<DBO, T>): List<T>

    fun find(specification: S, mapper: Function<DBO, T>): T?
}

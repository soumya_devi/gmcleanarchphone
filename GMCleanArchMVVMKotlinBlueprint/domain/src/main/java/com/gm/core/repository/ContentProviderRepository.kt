/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.repository

import android.content.*
import android.database.Cursor
import android.os.RemoteException
import com.gm.cleanarchbp.domain.util.DomainLog
import com.gm.cleanarchbp.domain.util.DomainLog.DEBUG
import com.gm.core.annotation.ApplicationContext
import com.gm.core.repository.specification.ContentProviderSpecification
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.functions.Function
import java.util.*
import javax.inject.Inject


class ContentProviderRepository<T> @Inject constructor(
        @ApplicationContext context: Context,
        private val mContentProviderOperationFactory: ContentProviderOperationFactory
) : ReadableRepository<T, ContentProviderSpecification, Cursor>,
        StreamableRepository<T, ContentProviderSpecification, Cursor>,
        WritableRepository<T, ContentProviderSpecification, ContentValues> {

    private val mContentResolver: ContentResolver = context.contentResolver

    override fun findAll(specification: ContentProviderSpecification,
                         mapper: Function<Cursor, T>): List<T> {
        if (DEBUG) {
            DomainLog.d(specification.toString())
        }

        val cursor = mContentResolver.query(
                specification.uri,
                specification.projection,
                specification.selection,
                specification.selectionArgs,
                specification.sortOrder)

        val result = ArrayList<T>()
        if (cursor != null) {
            try {
                while (cursor.moveToNext()) {
                    val value = mapper.apply(cursor)
                    if (value != null) {
                        result.add(value)
                    }
                }
            } catch (e: Exception) {
                throw IllegalStateException("Mapping failed from cursor to Model Entity type")
            } finally {
                cursor.close()
            }
        }
        return result
    }

    override fun find(specification: ContentProviderSpecification,
                      mapper: Function<Cursor, T>): T? {
        if (DEBUG) {
            DomainLog.d(specification.toString())
        }

        val cursor = mContentResolver.query(
                specification.uri,
                specification.projection,
                specification.selection,
                specification.selectionArgs,
                specification.sortOrder)

        if (cursor != null) {
            try {
                if (cursor.moveToNext()) {
                    return mapper.apply(cursor)
                }
            } catch (e: Exception) {
                throw IllegalStateException("Mapping failed from cursor to Model Entity type")
            } finally {
                cursor.close()
            }
        }
        return null
    }

    override fun streamAll(specification: ContentProviderSpecification,
                           mapper: Function<Cursor, T>): Observable<T> {

        if (DEBUG) {
            DomainLog.d(specification.toString())
        }
        return Observable.create { emitter ->
            mContentResolver.query(
                    specification.uri,
                    specification.projection,
                    specification.selection,
                    specification.selectionArgs,
                    specification.sortOrder)?.use { cursor ->
                if (DEBUG) {
                    DomainLog.d("Reading cursor has " + cursor.count + " items")
                }
                while (cursor.moveToNext() && !emitter.isDisposed) {
                    val foundValue = mapper.apply(cursor)
                    if (foundValue == null) {
                        emitter.onError(NullPointerException("Mapper returned null"))
                        break
                    }
                    emitter.onNext(foundValue)
                }
            }
            if (!emitter.isDisposed) {
                emitter.onComplete()
            }
        }
    }

    override fun stream(specification: ContentProviderSpecification,
                        mapper: Function<Cursor, T>): Maybe<T> {
        if (DEBUG) {
            DomainLog.d(specification.toString())
        }

        return Maybe.fromCallable {

            var foundValue: T? = null
            mContentResolver.query(
                    specification.uri,
                    specification.projection,
                    specification.selection,
                    specification.selectionArgs,
                    specification.sortOrder)?.use { cursor ->
                if (cursor.moveToNext()) {
                    foundValue = mapper.apply(cursor)
                    if (foundValue == null) {
                        throw NullPointerException("Mapper returned null")
                    }
                    if (cursor.moveToNext()) {
                        if (DEBUG) {
                            DomainLog.d("Cursor returned more than one item")
                        }
                    }
                }
            }
            foundValue
        }
    }


    override fun add(item: T,
                     specification: ContentProviderSpecification,
                     mapper: Function<T, ContentValues>): Boolean {

        if (DEBUG) {
            DomainLog.d(specification.toString())
        }

        try {
            val values = mapper.apply(item)
            val newUri = mContentResolver.insert(specification.uri, values)
            return newUri != null
        } catch (e: Exception) {
            throw IllegalStateException("Mapping failed from Model Entity type to content values", e)
        }

    }

    override fun addAll(items: Iterable<T>,
                        specification: ContentProviderSpecification,
                        mapper: Function<T, ContentValues>): Boolean {
        if (DEBUG) {
            DomainLog.d(specification.toString())
        }

        requireNotNull(specification.uri.authority) { "Specification must provide a uri with an authority for bulk" + "insertions" }

        val iterator = items.iterator()
        if (!iterator.hasNext()) {
            return false
        }

        val operations = ArrayList<ContentProviderOperation>()
        while (iterator.hasNext()) {
            val item = iterator.next()
            try {
                val contentValues = mapper.apply(item)
                val insertOperation = mContentProviderOperationFactory.createInsertOperation(specification.uri, contentValues)
                operations.add(insertOperation)
            } catch (e: Exception) {
                throw IllegalStateException("Mapping failed from Model Entity type to content values", e)
            }

        }
        return try {
            val result = mContentResolver.applyBatch(specification.uri.authority!!, operations)
            result.size == operations.size
        } catch (e: RemoteException) {
            DomainLog.e("Transaction error", e)
            false
        } catch (e: OperationApplicationException) {
            DomainLog.e("Transaction error", e)
            false
        }

    }

    override fun update(item: T,
                        specification: ContentProviderSpecification,
                        mapper: Function<T, ContentValues>): Int {
        if (DEBUG) {
            DomainLog.d(specification.toString())
        }

        try {
            val values = mapper.apply(item)
            return mContentResolver.update(
                    specification.uri,
                    values,
                    specification.selection,
                    specification.selectionArgs)
        } catch (e: Exception) {
            throw IllegalStateException("Mapping failed from Model Entity type to content values", e)
        }

    }


    override fun update(specification: ContentProviderSpecification, contentValue: ContentValues): Int {

        if (DEBUG) {
            DomainLog.d(specification.toString())
        }

        try {
            return mContentResolver.update(
                    specification.uri,
                    contentValue,
                    specification.selection,
                    specification.selectionArgs)
        } catch (e: Exception) {
            throw IllegalStateException("Exception occured while trying to update values", e)
        }

    }

    override fun remove(specification: ContentProviderSpecification): Int {
        if (DEBUG) {
            DomainLog.d(specification.toString())
        }

        return mContentResolver.delete(
                specification.uri,
                specification.selection,
                specification.selectionArgs)
    }
}

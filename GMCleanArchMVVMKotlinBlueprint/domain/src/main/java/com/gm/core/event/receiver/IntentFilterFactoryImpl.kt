/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.event.receiver

import android.content.IntentFilter

import javax.inject.Inject

class IntentFilterFactoryImpl @Inject
constructor()// required by dagger
    : IntentFilterFactory {

    override fun createIntentFilter(vararg actionableIntentType: ActionableIntentType): IntentFilter {
        require(actionableIntentType.isNotEmpty()) { "At least one ActionableIntentType must be " + "provided" }
        val intentFilter = IntentFilter()
        for (curActionType in actionableIntentType) {
            intentFilter.addAction(curActionType.action)
        }
        return intentFilter
    }
}

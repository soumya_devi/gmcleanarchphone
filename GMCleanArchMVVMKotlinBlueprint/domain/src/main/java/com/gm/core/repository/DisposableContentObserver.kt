/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.repository

import android.content.ContentResolver
import android.database.ContentObserver
import android.net.Uri
import android.os.Handler
import com.gm.cleanarchbp.domain.util.DomainLog
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import java.util.concurrent.atomic.AtomicBoolean

class DisposableContentObserver
/**
 * Creates a content observer.
 *
 * @param handler         The handler to run [.onChange] on, or null if none.
 * @param contentResolver provides access to content provider to unsubscribe on dispose
 * @param observer        the observer that this class will emit to.
 */
internal constructor(
        handler: Handler,
        private val mContentResolver: ContentResolver,
        private val mObserver: Observer<in Boolean>) : ContentObserver(handler), Disposable {

    private val mDisposed: AtomicBoolean = AtomicBoolean(false)

    override fun dispose() {
        if (mDisposed.compareAndSet(false, true)) {
            mContentResolver.unregisterContentObserver(this)
        }
    }

    override fun isDisposed(): Boolean {
        return mDisposed.get()
    }

    override fun onChange(selfChange: Boolean) {
        onChange(selfChange, null)
    }

    override fun onChange(selfChange: Boolean, uri: Uri?) {
        if (!isDisposed) {
            if (DomainLog.DEBUG) {
                DomainLog.d("Notified data changed on Uri: " + uri!!)
            }
            mObserver.onNext(selfChange)
        }
    }
}

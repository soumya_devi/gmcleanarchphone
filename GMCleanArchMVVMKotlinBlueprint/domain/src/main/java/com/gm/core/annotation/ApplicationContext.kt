/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.annotation

import javax.inject.Qualifier

/**
 * Qualifier allowing dagger to make a difference between the activity context
 * and the application context
 * It serves the same behavior as using @Named("application.context")
 */
@MustBeDocumented
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
@Qualifier
annotation class ApplicationContext

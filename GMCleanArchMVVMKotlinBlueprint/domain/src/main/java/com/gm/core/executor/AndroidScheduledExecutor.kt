/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.executor


import android.os.HandlerThread
import com.gm.cleanarchbp.domain.BuildConfig.APP_NAME
import io.reactivex.Scheduler
import io.reactivex.android.schedulers.AndroidSchedulers
import java.util.concurrent.atomic.AtomicInteger
import javax.inject.Inject

class AndroidScheduledExecutor @Inject
internal constructor() : ScheduledExecutor {

    private val mThreadNumber: AtomicInteger = AtomicInteger(1)

    override val mainThreadScheduler: Scheduler
        get() = AndroidSchedulers.mainThread()

    override val backgroundScheduler: Scheduler
        get() {
            val namePrefix = THREAD_PREFIX + mThreadNumber.getAndIncrement()
            val handlerThread = HandlerThread(namePrefix)
            handlerThread.start()
            return AndroidSchedulers.from(handlerThread.looper)
        }

    companion object {
        private const val THREAD_PREFIX = APP_NAME + "Thread-"
    }
}

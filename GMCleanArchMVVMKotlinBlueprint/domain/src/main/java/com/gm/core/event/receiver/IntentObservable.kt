/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.event.receiver

import android.content.Context
import android.content.Intent
import android.content.IntentFilter

import io.reactivex.Observable
import io.reactivex.Observer

/**
 * IntentObservable is in charge of registering a disposable receiver (Rx version of a
 * broadcast receiver) and to bind it to an observable.
 * When the subscribe method will be called on the observable, subscribeActual will be consequently
 * called.
 */
class IntentObservable private constructor(private val mContext: Context, private val mIntentFilter: IntentFilter) : Observable<Intent>() {

    override fun subscribeActual(observer: Observer<in Intent>) {
        val disposableReceiver = DisposableReceiver(mContext, observer)
        mContext.registerReceiver(disposableReceiver, mIntentFilter)
        observer.onSubscribe(disposableReceiver)
    }

    companion object {

        fun fromIntentFilter(context: Context,
                             intentFilter: IntentFilter): IntentObservable {
            return IntentObservable(context, intentFilter)
        }
    }
}

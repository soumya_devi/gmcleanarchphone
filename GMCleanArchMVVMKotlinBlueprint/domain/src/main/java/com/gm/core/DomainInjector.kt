/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */
package com.gm.core

import com.gm.cleanarchbp.domain.DaggerDomainComponent
import com.gm.cleanarchbp.domain.DomainComponent
import com.gm.cleanarchbp.model.ModelComponent
import com.gm.core.annotation.ModelComponentBuilder

/**
 * Domain Injector is a class used through reflection by the model.
 * It is used to bind the domain module and the model module together.
 * DO NOT CHANGE THE LOCATION OF THIS CLASS FOR ANY REASON
 */
object DomainInjector {

    private var sDomainComponent: DomainComponent? = null

    val domainComponent: DomainComponent?
        get() {
            checkNotNull(sDomainComponent) { "Attempt to access the domain component before it has been built" }
            return sDomainComponent
        }

    /**
     * Method found through annotation defined in the model.
     * It provides the application instance to the domain component
     *
     * @param application the application instance (built in presentation)
     * @return the built DomainComponent (specialization of ModelComponent)
     */
    @ModelComponentBuilder
    fun buildDomainComponent(application: Any): ModelComponent? {
        sDomainComponent = DaggerDomainComponent.factory().create(application)
        return sDomainComponent
    }
}

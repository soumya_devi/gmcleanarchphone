/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.event.bus


import com.gm.cleanarchbp.domain.util.DomainLog
import com.gm.cleanarchbp.domain.util.DomainLog.DEBUG
import com.gm.core.event.Event
import com.gm.core.event.EventBus
import com.gm.core.event.EventSubscriber
import com.gm.core.executor.ScheduledExecutor
import io.reactivex.Scheduler
import io.reactivex.functions.Consumer
import java.util.concurrent.ConcurrentHashMap
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Implementation of an eventBus using RxBus. RxBus runs by default on the main
 * thread scheduler. Therefore every event will be received on the main thread.
 */
@Singleton
class RxBus @Inject
internal constructor(scheduledExecutor: ScheduledExecutor) : EventBus {

    private val publisherMap: MutableMap<Class<out Event>, BusPublisher> = ConcurrentHashMap()
    private val subscriptionMap: MutableMap<EventSubscriber, EventTypeCompositeDisposable> = ConcurrentHashMap()
    private val observeOnScheduler: Scheduler = scheduledExecutor.mainThreadScheduler

    override fun post(event: Event) {
        if (DEBUG) {
            DomainLog.d(event.toString())
        }
        retrievePublisher(event.javaClass).publish(event)
    }

    @Synchronized
    override fun <T : Event> register(eventType: Class<T>,
                                      eventSubscriber: EventSubscriber,
                                      consumer: Consumer<in T>) {
        val busPublisher = retrievePublisher(eventType)
        val disposable = busPublisher.subscribeWithoutCache(eventType, consumer)
        retrieveSubscription(eventSubscriber).add(eventType, disposable)
    }

    @Synchronized
    override fun <T : Event> register(eventType: Class<T>,
                                      eventSubscriber: EventSubscriber,
                                      consumer: (T) -> Unit) {
        val busPublisher = retrievePublisher(eventType)
        val disposable = busPublisher.subscribeWithoutCache(eventType, Consumer { consumer(it) })
        retrieveSubscription(eventSubscriber).add(eventType, disposable)
    }

    @Synchronized
    override fun unregister(eventSubscriber: EventSubscriber) {
        /*
         * When unregistering an event subscriber, all the subscribed observables (disposable) are disposed.
         * All the other event subscribers will still receive events.
         * When no more event subscribers listen to a specific event type, the publisher still remains to be able to
         * cache events. If optimization is required later on, a LRU replacement policy for publishers shall be used.
         */
        val eventTypeCompositeDisposable = subscriptionMap.remove(eventSubscriber)
        eventTypeCompositeDisposable?.dispose()
    }

    override fun <T : Event> unregister(eventType: Class<T>,
                                        eventSubscriber: EventSubscriber) {
        val eventTypeCompositeDisposable = subscriptionMap[eventSubscriber]
        if (eventTypeCompositeDisposable != null) {
            eventTypeCompositeDisposable.dispose(eventType)
            if (eventTypeCompositeDisposable.isEmpty) {
                subscriptionMap.remove(eventSubscriber)
            }
        }
    }

    private fun retrieveSubscription(eventSubscriber: EventSubscriber): EventTypeCompositeDisposable {
        val existingDisposable = subscriptionMap[eventSubscriber]
        if (existingDisposable != null) {
            return existingDisposable
        }

        val compositeDisposable = EventTypeCompositeDisposable()
        subscriptionMap[eventSubscriber] = compositeDisposable
        return compositeDisposable
    }

    private fun retrievePublisher(eventType: Class<out Event>): BusPublisher {
        val existingPublisher = publisherMap[eventType]
        if (existingPublisher != null) {
            return existingPublisher
        }

        val newBusPublisher = BusPublisher.create(observeOnScheduler)
        publisherMap[eventType] = newBusPublisher
        return newBusPublisher
    }
}

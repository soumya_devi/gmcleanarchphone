/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.event.bus

import com.gm.cleanarchbp.domain.util.DomainLog
import com.gm.core.event.Event
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.*

internal class EventTypeCompositeDisposable {

    private val mEventTypeDisposableMap: MutableMap<Class<out Event>, CompositeDisposable>

    val isEmpty: Boolean
        get() = mEventTypeDisposableMap.isEmpty()

    init {
        mEventTypeDisposableMap = HashMap()
    }

    fun <T : Event> add(eventType: Class<T>, disposable: Disposable) {
        when (val existingCompositeDisposable = mEventTypeDisposableMap[eventType]) {
            null -> mEventTypeDisposableMap[eventType] = CompositeDisposable(disposable)
            else -> existingCompositeDisposable.add(disposable)
        }
    }

    fun <T : Event> dispose(eventType: Class<T>) {
        val removedCompositeDisposable = mEventTypeDisposableMap.remove(eventType)
        if (removedCompositeDisposable != null) {
            removedCompositeDisposable.dispose()
        } else {
            DomainLog.e("Could not find the provided event type in order to dispose it")
        }
    }

    fun dispose() {
        mEventTypeDisposableMap.values.forEach { it.dispose() }
        mEventTypeDisposableMap.clear()
    }
}

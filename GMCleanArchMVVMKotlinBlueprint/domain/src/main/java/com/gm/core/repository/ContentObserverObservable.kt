/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.repository

import android.content.ContentResolver
import android.content.Context
import android.net.Uri
import android.os.Handler

import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers

/**
 *
 */
class ContentObserverObservable private constructor(
        context: Context,
        private val mUri: Uri,
        private val mNotifyForDescendants: Boolean
) : Observable<Boolean>() {

    private val mContentResolver: ContentResolver = context.contentResolver

    override fun subscribeActual(observer: Observer<in Boolean>) {
        val disposableContentObserver = DisposableContentObserver(
                Handler(),
                mContentResolver,
                observer
        )
        mContentResolver.registerContentObserver(mUri, mNotifyForDescendants, disposableContentObserver)
        observer.onSubscribe(disposableContentObserver)
    }

    companion object {

        fun fromUri(context: Context,
                    uri: Uri,
                    notifyForDescendants: Boolean): Observable<Boolean> {
            return ContentObserverObservable(context, uri, notifyForDescendants)
                    .subscribeOn(AndroidSchedulers.mainThread())
        }
    }
}

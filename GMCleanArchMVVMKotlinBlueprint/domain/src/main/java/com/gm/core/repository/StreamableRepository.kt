/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.repository

import com.gm.core.repository.specification.Specification
import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.functions.Function

/**
 * Defines the contract for a reactive data retrieval component.
 * The non-reactive interface is here [com.gm.core.repository.ReadableRepository]
 *
 * @param <T>   The Entity Object type
 * @param <S>   The Specification / Query type
 * @param <DBO> The Database object type
</DBO></S></T> */
interface StreamableRepository<T, S : Specification, DBO> {

    fun streamAll(specification: S, mapper: Function<DBO, T>): Observable<T>

    /**
     * The reactive version of [ReadableRepository.find]
     *
     * @param specification Defines the way to retrieve data (e.g. a query)
     * @param mapper        Define a way to transform the database object into an entity object. (e.g. Cursor to Favorite)
     * @return a [Maybe] streaming one item if one is found or nothing if not.
     */
    fun stream(specification: S, mapper: Function<DBO, T>): Maybe<T>
}

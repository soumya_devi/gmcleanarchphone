/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.repository.specification

/**
 * Parent interface for a specification
 * Used as a strategy design pattern in the repository design pattern
 */
interface Specification

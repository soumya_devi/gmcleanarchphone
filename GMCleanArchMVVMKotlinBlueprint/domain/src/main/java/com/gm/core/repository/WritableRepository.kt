/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.repository

import com.gm.core.repository.specification.Specification

import io.reactivex.functions.Function

/**
 * Define what a writable source of data should be
 *
 * @param <T>   Define the input entity type used in this repository
 * @param <S>   Specification about how the data should be written/removed (Strategy)
 * @param <DBO> Define the stored data entity type
</DBO></S></T> */
interface WritableRepository<T, S : Specification, DBO> {

    /**
     * add a given item to the repository given a specification
     *
     * @param item          the item to be inserted
     * @param specification where it should be inserted
     * @param mapper        how the item should be converted
     * @return true if successful and false if failed
     */
    fun add(item: T, specification: S, mapper: Function<T, DBO>): Boolean

    /**
     * addAll multiple items as a transaction
     *
     * @param items         iterable of items to be inserted
     * @param specification where they should be inserted
     * @param mapper        convert every item into a storable object
     * @return true if all insertions are successful, false otherwise
     */
    fun addAll(items: Iterable<T>, specification: S, mapper: Function<T, DBO>): Boolean

    /**
     * update a given item
     *
     * @param item          the new item
     * @param specification where it should be updated
     * @param mapper        how the item should be converted
     * @return the number of updated rows
     */
    fun update(item: T, specification: S, mapper: Function<T, DBO>): Int

    /**
     * update all values matching a specification
     *
     * @param specification which pattern of item(s) to be updated
     * @param contentValue  the new values
     * @return the number of updated rows
     */
    fun update(specification: S, contentValue: DBO): Int

    /**
     * remove a items matching a specification
     *
     * @param specification which pattern of item(s) should be remove
     * @return the number of deleted rows
     */
    fun remove(specification: S): Int
}

/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */
package android.util;

/**
 * Used to stub android logger to prevent a not mocked exception
 * Could be solved with a testOption in gradle file but this option should be used with caution
 */
public class Log {
    public static int d(String tag, String msg) {
        return 0;
    }

    public static int i(String tag, String msg) {
        return 0;
    }

    public static int w(String tag, String msg) {
        return 0;
    }

    public static int e(String tag, String msg) {
        return 0;
    }

    public static boolean isLoggable(String tag, int level) {
        return false;
    }
}

/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */
package com.gm.core.event.bus

import com.gm.core.event.Event
import com.gm.core.event.EventSubscriber
import com.gm.core.executor.ScheduledExecutor
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.functions.Consumer
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.junit.Test

class RxBusTest : EventSubscriber {

    private val mockScheduledExecutor = mockk<ScheduledExecutor>(relaxed = true)
    private val mockConsumer = mockk<Consumer<FakeEvent>>(relaxed = true)

    private lateinit var mTestScheduler: TestScheduler
    private lateinit var mRxBus: RxBus

    @Before
    fun setUp() {
        mTestScheduler = TestScheduler()

        every { mockScheduledExecutor.mainThreadScheduler } returns mTestScheduler

        mRxBus = RxBus(mockScheduledExecutor)
    }

    @Test
    @Throws(Exception::class)
    fun register_whenConsumerRegistersBeforePost_consumerReceivesPostedValue() {
        mRxBus.register(FakeEvent::class.java, this, mockConsumer)
        mRxBus.post(A_FAKE_EVENT)

        mTestScheduler.triggerActions()

        verify { mockConsumer.accept(A_FAKE_EVENT) }
    }

    @Test
    @Throws(Exception::class)
    fun register_whenConsumerRegistersAfterPost_consumerDoesNotReceiveAnyValue() {
        mRxBus.post(A_FAKE_EVENT)
        mRxBus.register(FakeEvent::class.java, this, mockConsumer)

        mTestScheduler.triggerActions()

        verify(exactly = 0) { mockConsumer.accept(A_FAKE_EVENT) }
    }

    @Test
    @Throws(Exception::class)
    fun unregister_whenEventIsPostedBeforeUnregister_consumerReceivesPostedValue() {
        mRxBus.register(FakeEvent::class.java, this, mockConsumer)
        mRxBus.post(A_FAKE_EVENT)

        mTestScheduler.triggerActions()

        mRxBus.unregister(this)

        mTestScheduler.triggerActions()

        verify { mockConsumer.accept(A_FAKE_EVENT) }
    }

    @Test
    @Throws(Exception::class)
    fun unregister_whenEventIsPostedAfterUnregister_consumerDoesNotReceiveAnyValue() {
        mRxBus.register(FakeEvent::class.java, this, mockConsumer)
        mRxBus.unregister(this)

        mTestScheduler.triggerActions()

        mRxBus.post(A_FAKE_EVENT)

        mTestScheduler.triggerActions()

        verify(exactly = 0) { mockConsumer.accept(A_FAKE_EVENT) }
    }

    @Test
    @Throws(Exception::class)
    fun unregisterEventType_whenEventIsPostedBeforeUnregister_consumerReceivesPostedValue() {
        mRxBus.register(FakeEvent::class.java, this, mockConsumer)
        mRxBus.post(A_FAKE_EVENT)

        mTestScheduler.triggerActions()

        mRxBus.unregister(FakeEvent::class.java, this)

        mTestScheduler.triggerActions()

        verify { mockConsumer.accept(A_FAKE_EVENT) }
    }

    @Test
    @Throws(Exception::class)
    fun unregisterEventType_whenEventIsPostedAfterUnregister_consumerDoesNotReceiveAnyValue() {
        mRxBus.register(FakeEvent::class.java, this, mockConsumer)
        mRxBus.unregister(FakeEvent::class.java, this)

        mTestScheduler.triggerActions()

        mRxBus.post(A_FAKE_EVENT)

        mTestScheduler.triggerActions()

        verify(exactly = 0) { mockConsumer.accept(A_FAKE_EVENT) }
    }

    private class FakeEvent : Event

    companion object {

        private val A_FAKE_EVENT = FakeEvent()
    }
}
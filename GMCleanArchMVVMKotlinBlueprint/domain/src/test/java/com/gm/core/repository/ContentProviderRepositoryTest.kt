/*
 * Copyright (C) GM Global Technology Operations LLC 2019
 * All Rights Reserved.
 * GM Confidential Restricted.
 */

package com.gm.core.repository


import android.content.*
import android.database.Cursor
import android.net.Uri
import android.os.RemoteException
import com.gm.core.repository.specification.ContentProviderSpecification
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.functions.Function
import io.reactivex.observers.TestObserver
import org.hamcrest.CoreMatchers.*
import org.hamcrest.collection.IsEmptyCollection.empty
import org.junit.Assert.assertThat
import org.junit.Before
import org.junit.Test
import java.util.*

class ContentProviderRepositoryTest<T> {
    private val mTestObserver = TestObserver<T>()
    private val mStringTestObserver = TestObserver<String>()

    private val mockContentProviderSpecification = mockk<ContentProviderSpecification>(relaxed = true)
    private val mockContext = mockk<Context>(relaxed = true)
    private val mockContentProviderOperationFactory = mockk<ContentProviderOperationFactory>(relaxed = true)
    private val mockContentResolver = mockk<ContentResolver>(relaxed = true)
    private val mockCursorMapper = mockk<Function<Cursor, T>>(relaxed = true)
    private val mockCursorStringMapper = mockk<Function<Cursor, String>>(relaxed = true)
    private val mockCursor = mockk<Cursor>(relaxed = true)

    private val mockStringContentValuesMapper = mockk<Function<String, ContentValues>>(relaxed = true)

    private lateinit var mContentProviderRepository: ContentProviderRepository<T>
    private lateinit var mStringContentProviderRepository: ContentProviderRepository<String>

    @Before
    fun setUp() {
        every { mockContext.contentResolver } returns mockContentResolver
        stub(mockContentProviderSpecification)
        every { mockContentResolver.query(any(), any(), any(), any(), any()) } returns mockCursor
        mContentProviderRepository = ContentProviderRepository(mockContext, mockContentProviderOperationFactory)
        mStringContentProviderRepository = ContentProviderRepository(mockContext, mockContentProviderOperationFactory)
    }

    @Test
    fun findAll_whenCursorIsNull_returnsEmptyList() {
        // init
        val nullCursor: Cursor? = null
        every { mockContentResolver.query(any(), any(), any(), any(), any()) } returns nullCursor

        // run
        val actualResult = mContentProviderRepository.findAll(mockContentProviderSpecification, mockCursorMapper)

        // verify
        assertThat(actualResult, `is`(empty()))
    }

    @Test(expected = IllegalStateException::class)
    fun findAll_whenMapperThrowsException_throwsIllegalStateException() {
        // init
        every { mockCursor.moveToNext() } returns true andThen false
        every { mockCursorMapper.apply(mockCursor) } throws Exception()

        // run
        mContentProviderRepository.findAll(mockContentProviderSpecification, mockCursorMapper)
    }

    @Test
    fun findAll_whenCursorIsNotNull_closesTheCursor() {
        // init
        every { mockCursor.moveToNext() } returns false

        // run
        mContentProviderRepository.findAll(mockContentProviderSpecification, mockCursorMapper)

        // verify
        verify { mockCursor.close() }
    }

    @Test
    fun findAll_whenMapperReturnsNull_returnsEmptyList() {
        // init
        val mockMappedValue: T? = null

        every { mockCursor.moveToNext() } returns true andThen false

        every { mockCursorMapper.apply(mockCursor) } returns mockMappedValue

        //run
        val actualResult = mContentProviderRepository.findAll(
                mockContentProviderSpecification,
                mockCursorMapper)

        //verify
        assertThat(actualResult, `is`(empty()))
    }

    @Test
    fun findAll_whenMapperReturnsValidValue_returnsValidValue() {
        // init
        val mockMappedValue = A_STRING
        val expectedResult = ArrayList<String>()
        expectedResult.add(mockMappedValue)

        every { mockCursor.moveToNext() } returns true andThen false

        every { mockCursorStringMapper.apply(mockCursor) } returns mockMappedValue

        //run
        val actualResult = mStringContentProviderRepository.findAll(mockContentProviderSpecification, mockCursorStringMapper)

        //verify
        assertThat(actualResult, `is`<List<String>>(expectedResult))
    }

    @Test
    fun find_whenCursorIsNull_returnsEmptyList() {
        // init
        val nullCursor: Cursor? = null
        every { mockContentResolver.query(any(), any(), any(), any(), any()) } returns nullCursor

        // run
        val actualResult = mContentProviderRepository.find(mockContentProviderSpecification, mockCursorMapper)

        // verify
        assertThat<T>(actualResult, nullValue())
    }

    @Test(expected = IllegalStateException::class)
    fun find_whenMapperThrowsException_throwsIllegalStateException() {
        // init
        every { mockCursor.moveToNext() } returns true andThen false

        every { mockCursorMapper.apply(mockCursor) } throws Exception()

        // run
        mContentProviderRepository.find(mockContentProviderSpecification, mockCursorMapper)
    }

    @Test
    fun find_whenCursorIsNotNull_closesTheCursor() {
        // init
        every { mockCursor.moveToNext() } returns false

        // run
        mContentProviderRepository.find(mockContentProviderSpecification, mockCursorMapper)

        // verify
        verify { mockCursor.close() }
    }

    @Test
    fun find_whenMapperReturnsNull_returnsNull() {
        // init
        val mockMappedValue: T? = null

        every { mockCursor.moveToNext() } returns true andThen false

        every { mockCursorMapper.apply(mockCursor) } returns mockMappedValue

        // run
        val actualResult = mContentProviderRepository.find(mockContentProviderSpecification, mockCursorMapper)

        // verify
        assertThat<T>(actualResult, nullValue())
    }

    @Test
    fun find_whenReturnsValidItem_returnsValidValue() {
        // init
        val mockMappedValue = A_STRING

        every { mockCursorStringMapper.apply(mockCursor) } returns mockMappedValue

        every { mockCursor.moveToNext() } returns true andThen false

        // run
        val actualResult = mStringContentProviderRepository.find(mockContentProviderSpecification, mockCursorStringMapper)

        // verify
        assertThat<String>(actualResult, `is`(mockMappedValue))
    }

    @Test
    fun streamAll_whenNullCursorProvidedAndEmitterNotDisposed_onlyInvokesObserverOnComplete() {
        // init
        val nullCursor: Cursor? = null
        every { mockContentResolver.query(any(), any(), any(), any(), any()) } returns nullCursor

        // run
        mStringContentProviderRepository.streamAll(mockContentProviderSpecification, mockCursorStringMapper)
                .subscribe(mStringTestObserver)

        // verify
        mStringTestObserver.assertNoValues()
        mStringTestObserver.assertNoErrors()
        mStringTestObserver.assertComplete()
    }

    @Test
    fun streamAll_whenNullCursorAndObserverDisposed_neverInvokesObserverOnComplete() {
        // init
        val nullCursor: Cursor? = null
        every { mockContentResolver.query(any(), any(), any(), any(), any()) } returns nullCursor

        // run
        mStringTestObserver.dispose()
        mStringContentProviderRepository.streamAll(mockContentProviderSpecification, mockCursorStringMapper)
                .subscribe(mStringTestObserver)

        // verify
        mStringTestObserver.assertEmpty()
    }

    @Test
    fun streamAll_whenMapperThrowsException_invokesOnErrorOnObserverWithException() {
        // init
        every { mockCursor.moveToNext() } returns true andThen false

        every { mockCursorMapper.apply(mockCursor) } throws Exception()

        // run
        mContentProviderRepository.streamAll(mockContentProviderSpecification, mockCursorMapper)
                .subscribe(mTestObserver)

        // verify
        mTestObserver.assertError(Exception::class.java)
    }

    @Test
    fun streamAll_whenCursorIsNotNull_closesTheCursor() {
        // init
        every { mockCursor.moveToNext() } returns false

        // run
        mContentProviderRepository.streamAll(mockContentProviderSpecification, mockCursorMapper)
                .subscribe(mTestObserver)

        // verify
        verify { mockCursor.close() }
    }

    @Test
    fun streamAll_whenMapperReturnsNull_invokesObserverOnErrorWithNullPointerExceptionAndDoesNotComplete() {
        // init
        val mockMappedValue: T? = null

        every { mockCursor.moveToNext() } returns true andThen false

        every { mockCursorMapper.apply(mockCursor) } returns mockMappedValue

        // run
        mContentProviderRepository.streamAll(mockContentProviderSpecification, mockCursorMapper)
                .subscribe(mTestObserver)

        // verify
        mTestObserver.assertError(NullPointerException::class.java)
    }

    @Test
    fun streamAll_whenMapperReturnsValidValue_invokesObserverOnNextAndCompletes() {
        // init
        val mockMappedValue = A_STRING

        every { mockCursor.moveToNext() } returns true andThen false

        every { mockCursorStringMapper.apply(mockCursor) } returns mockMappedValue

        // run
        mStringContentProviderRepository.streamAll(mockContentProviderSpecification, mockCursorStringMapper)
                .subscribe(mStringTestObserver)

        // verify
        mStringTestObserver.assertResult(mockMappedValue)
    }

    @Test
    fun streamAll_whenObserverIsDisposed_invokesNothing() {
        // init
        every { mockCursor.moveToNext() } returns true andThen false

        // run
        mTestObserver.dispose()
        mContentProviderRepository.streamAll(mockContentProviderSpecification, mockCursorMapper)
                .subscribe(mTestObserver)

        // verify
        mTestObserver.assertEmpty()
    }

    @Test
    fun stream_whenObserverIsDisposed_invokesNothing() {
        // run
        mStringTestObserver.dispose()
        mStringContentProviderRepository.stream(mockContentProviderSpecification, mockCursorStringMapper)
                .subscribe(mStringTestObserver)

        // verify
        mStringTestObserver.assertEmpty()
    }

    @Test
    fun stream_whenMapperReturnsNull_invokesObserverOnError() {
        // init
        val mockMappedValue: T? = null

        every { mockCursor.moveToNext() } returns true andThen false
        every { mockCursorMapper.apply(mockCursor) } returns mockMappedValue

        // run
        mContentProviderRepository.stream(mockContentProviderSpecification, mockCursorMapper).subscribe(mTestObserver)

        // verify
        mTestObserver.assertError(NullPointerException::class.java)
    }

    @Test
    fun stream_whenCursorHasExactlyOneValidItem_invokesObserverOnNextAndCompletes() {
        // init
        val mockMappedValue = "String"

        every { mockCursor.moveToNext() } returns true andThen false
        every { mockCursorStringMapper.apply(mockCursor) } returns mockMappedValue

        // run
        mStringContentProviderRepository.stream(mockContentProviderSpecification, mockCursorStringMapper)
                .subscribe(mStringTestObserver)

        // verify
        mStringTestObserver.assertResult(mockMappedValue)
    }

    @Test
    fun stream_whenCursorIsNotNull_closesTheCursor() {
        // init
        every { mockCursor.moveToNext() } returns false

        // run
        mContentProviderRepository.stream(mockContentProviderSpecification, mockCursorMapper).subscribe(mTestObserver)

        // verify
        verify { mockCursor.close() }
    }

    @Test
    fun add_whenInsertionIsSuccessful_returnsSuccessfulCode() {

        // init
        val successCode = true

        val mockContentValues = mockk<ContentValues>(relaxed = true)
        val fakeInsertItem = A_STRING

        every { mockStringContentValuesMapper.apply(fakeInsertItem) } returns mockContentValues
        every {
            mockContentResolver.insert(
                    mockContentProviderSpecification.uri,
                    mockContentValues)
        } returns A_URI

        // run
        val actualCode = mStringContentProviderRepository.add(
                fakeInsertItem,
                mockContentProviderSpecification,
                mockStringContentValuesMapper)

        // verify
        assertThat(actualCode, `is`(successCode))
    }

    @Test
    fun add_whenInsertionFails_returnsFailureCode() {

        // init
        val failureCode = false

        val mockContentValues = mockk<ContentValues>()
        val fakeInsertItem = A_STRING


        every { mockStringContentValuesMapper.apply(fakeInsertItem) } returns mockContentValues
        every {
            mockContentResolver.insert(
                    mockContentProviderSpecification.uri,
                    mockContentValues)
        } returns null

        // run
        val actualCode = mStringContentProviderRepository.add(
                fakeInsertItem,
                mockContentProviderSpecification,
                mockStringContentValuesMapper)

        // verify
        assertThat(actualCode, `is`(failureCode))
    }

    @Test(expected = IllegalStateException::class)
    fun add_whenMapperThrowsException_throwsException() {

        // init
        val fakeInsertItem = A_STRING

        every { mockStringContentValuesMapper.apply(fakeInsertItem) } throws Exception()

        // run
        mStringContentProviderRepository.add(
                fakeInsertItem,
                mockContentProviderSpecification,
                mockStringContentValuesMapper)
    }

    @Test
    fun addAll_whenInsertionIsSuccessful_returnsSuccessfulCode() {

        // init
        val successCode = true

        val mockContentValues = mockk<ContentValues>()
        val fakeTwoItemList = listOf("1 string", "two strings")
        val mockOperation = mockk<ContentProviderOperation>()
        val fakeTwoItemResult = arrayOf(ContentProviderResult(A_URI), ContentProviderResult(A_URI))

        every {
            mockContentProviderOperationFactory.createInsertOperation(A_URI, mockContentValues)
        } returns mockOperation
        every { mockStringContentValuesMapper.apply(any()) } returns mockContentValues
        every { mockContentResolver.applyBatch(any(), any()) } returns fakeTwoItemResult

        // run
        val actualCode = mStringContentProviderRepository.addAll(
                fakeTwoItemList,
                mockContentProviderSpecification,
                mockStringContentValuesMapper)

        // verify
        assertThat(actualCode, `is`(successCode))
    }

    @Test
    @Throws(Exception::class)
    fun addAll_whenInsertionFails_returnsFailureCode() {

        val failureCode = false

        val mockContentValues = mockk<ContentValues>()
        val fakeTwoItemList = listOf("1 string", "two strings")
        val mockOperation = mockk<ContentProviderOperation>()

        //if an array of one item is returned as a result for a two item array input, the transaction has failed
        val fakeOneItemResult = arrayOf(ContentProviderResult(A_URI))

        every {
            mockContentProviderOperationFactory.createInsertOperation(A_URI, mockContentValues)
        } returns mockOperation
        every { mockStringContentValuesMapper.apply(any()) } returns mockContentValues
        every { mockContentResolver.applyBatch(any(), any()) } returns fakeOneItemResult

        // run
        val actualCode = mStringContentProviderRepository.addAll(
                fakeTwoItemList,
                mockContentProviderSpecification,
                mockStringContentValuesMapper)

        // verify
        assertThat(actualCode, `is`(failureCode))
    }


    @Test
    fun addAll_whenApplyBatchThrowsRemoteException_returnsFailureCode() {

        val failureCode = false

        val mockContentValues = mockk<ContentValues>()
        val fakeTwoItemList = listOf("1 string", "two strings")
        val mockOperation = mockk<ContentProviderOperation>()

        every {
            mockContentProviderOperationFactory.createInsertOperation(A_URI, mockContentValues)
        } returns mockOperation
        every { mockStringContentValuesMapper.apply(any()) } returns mockContentValues
        every { mockContentResolver.applyBatch(any(), any()) } throws RemoteException()

        // run
        val actualCode = mStringContentProviderRepository.addAll(
                fakeTwoItemList,
                mockContentProviderSpecification,
                mockStringContentValuesMapper)

        // verify
        assertThat(actualCode, `is`(failureCode))
    }

    @Test
    @Throws(Exception::class)
    fun addAll_whenApplyBatchThrowsOperationApplicationException_returnsFailureCode() {

        val failureCode = false

        val mockContentValues = mockk<ContentValues>()
        val fakeTwoItemList = listOf("1 string", "two strings")
        val mockOperation = mockk<ContentProviderOperation>()

        every {
            mockContentProviderOperationFactory.createInsertOperation(A_URI, mockContentValues)
        } returns mockOperation
        every { mockStringContentValuesMapper.apply(any()) } returns mockContentValues
        every { mockContentResolver.applyBatch(any(), any()) } throws OperationApplicationException()

        // run
        val actualCode = mStringContentProviderRepository.addAll(
                fakeTwoItemList,
                mockContentProviderSpecification,
                mockStringContentValuesMapper)

        // verify
        assertThat(actualCode, `is`(failureCode))
    }

    @Test(expected = IllegalArgumentException::class)
    fun addAll_whenAuthorityNotProvided_throwsException() {

        // init
        val fakeTwoItemList = listOf("1 string", "two strings")

        every { mockContentProviderSpecification.uri.authority } returns null

        // run
        mStringContentProviderRepository.addAll(
                fakeTwoItemList,
                mockContentProviderSpecification,
                mockStringContentValuesMapper)
    }

    @Test(expected = IllegalStateException::class)
    @Throws(Exception::class)
    fun addAll_whenMapperThrowsException_throwsException() {

        // init
        val fakeTwoItemList = listOf("1 string", "two strings")

        every { mockStringContentValuesMapper.apply(any()) } throws Exception()

        // run
        mStringContentProviderRepository.addAll(
                fakeTwoItemList,
                mockContentProviderSpecification,
                mockStringContentValuesMapper)
    }

    @Test
    @Throws(Exception::class)
    fun updateItem_whenUpdateSucceeds_returnNumberOfRowUpdated() {

        // init
        val numberOfRowUpdated = 2

        val mockContentValues = mockk<ContentValues>()
        val fakeInsertItem = A_STRING

        every { mockStringContentValuesMapper.apply(fakeInsertItem) } returns mockContentValues
        every {
            mockContentResolver.update(
                    any(),
                    mockContentValues,
                    any(),
                    any())
        } returns numberOfRowUpdated

        // run
        val actualNumberOfRowUpdated = mStringContentProviderRepository.update(
                fakeInsertItem,
                mockContentProviderSpecification,
                mockStringContentValuesMapper)

        // verify
        assertThat(actualNumberOfRowUpdated, `is`(equalTo(numberOfRowUpdated)))
    }

    @Test
    @Throws(Exception::class)
    fun updateItem_whenUpdateFails_returnZero() {

        // init
        val numberOfRowUpdated = 0

        val mockContentValues = mockk<ContentValues>()
        val fakeInsertItem = A_STRING

        every { mockStringContentValuesMapper.apply(fakeInsertItem) } returns mockContentValues
        every {
            mockContentResolver.update(
                    mockContentProviderSpecification.uri,
                    mockContentValues,
                    mockContentProviderSpecification.selection,
                    mockContentProviderSpecification.selectionArgs)
        } returns numberOfRowUpdated

        // run
        val actualNumberOfRowUpdated = mStringContentProviderRepository.update(
                fakeInsertItem,
                mockContentProviderSpecification,
                mockStringContentValuesMapper)

        // verify
        assertThat(actualNumberOfRowUpdated, `is`(equalTo(numberOfRowUpdated)))
    }

    @Test(expected = IllegalStateException::class)
    @Throws(Exception::class)
    fun updateItem_whenMapperThrowsException_throwsException() {

        // init
        val fakeInsertItem = A_STRING

        every { mockStringContentValuesMapper.apply(fakeInsertItem) } throws Exception()


        // run
        mStringContentProviderRepository.update(
                fakeInsertItem,
                mockContentProviderSpecification,
                mockStringContentValuesMapper)
    }

    @Test
    fun update_whenUpdateSucceeds_returnsNumberOfRowsUpdated() {
        val numberOfRowUpdated = 2

        val mockContentValues = mockk<ContentValues>()

        every {
            mockContentResolver.update(
                    any(),
                    mockContentValues,
                    any(),
                    any())
        } returns numberOfRowUpdated

        val actualNumberOfRowUpdated = mStringContentProviderRepository.update(
                mockContentProviderSpecification, mockContentValues)

        assertThat(actualNumberOfRowUpdated, `is`(equalTo(numberOfRowUpdated)))
    }

    @Test
    fun update_whenUpdateFails_returnZero() {

        val numberOfRowUpdated = 0

        val mockContentValues = mockk<ContentValues>()

        every {
            mockContentResolver.update(
                    mockContentProviderSpecification.uri,
                    mockContentValues,
                    mockContentProviderSpecification.selection,
                    mockContentProviderSpecification.selectionArgs)
        } returns numberOfRowUpdated

        val actualNumberOfRowUpdated = mStringContentProviderRepository.update(
                mockContentProviderSpecification, mockContentValues)

        assertThat(actualNumberOfRowUpdated, `is`(equalTo(numberOfRowUpdated)))
    }

    @Test(expected = IllegalStateException::class)
    @Throws(Exception::class)
    fun update_whenUpdateThrowsException_throwsException() {

        val mockContentValues = mockk<ContentValues>()
        every {
            mockContentResolver.update(
                    any(),
                    mockContentValues,
                    any(),
                    any())
        } throws RuntimeException()

        mStringContentProviderRepository.update(mockContentProviderSpecification, mockContentValues)
    }

    @Test
    fun removeItem_whenDeleteSucceeds_returnNumberOfRowDeleted() {

        // init
        val numberOfRowDeleted = 2

        every { mockContentResolver.delete(any(), any(), any()) } returns numberOfRowDeleted

        // run
        val actualNumberOfRowDeleted = mStringContentProviderRepository.remove(mockContentProviderSpecification)

        // verify
        assertThat(actualNumberOfRowDeleted, `is`(equalTo(numberOfRowDeleted)))
    }

    @Test
    fun removeItem_whenDeleteFails_returnZero() {

        // init
        val numberOfRowDeleted = 0


        every {
            mockContentResolver.delete(
                    mockContentProviderSpecification.uri,
                    mockContentProviderSpecification.selection,
                    mockContentProviderSpecification.selectionArgs)
        } returns numberOfRowDeleted

        // run
        val actualNumberOfRowDeleted = mStringContentProviderRepository.remove(mockContentProviderSpecification)

        // verify
        assertThat(actualNumberOfRowDeleted, `is`(equalTo(numberOfRowDeleted)))
    }

    companion object {

        private const val A_STRING = "a string"
        private val A_URI = mockk<Uri>()
        private val A_STRING_ARRAY = arrayOf("a string item", "another string item")

        private fun stub(contentProviderSpecification: ContentProviderSpecification) {
            every { contentProviderSpecification.uri } returns A_URI
            every { contentProviderSpecification.projection } returns A_STRING_ARRAY
            every { contentProviderSpecification.selection } returns A_STRING
            every { contentProviderSpecification.selectionArgs } returns A_STRING_ARRAY
            every { contentProviderSpecification.sortOrder } returns A_STRING
            every { contentProviderSpecification.uri.authority } returns A_STRING
        }
    }
}
